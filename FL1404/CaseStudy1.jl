using PyPlot

include("../Aircraft/Aircraft.jl")
include("../Controller/PilotModelFL1404.jl")
include("../Controller/EAcontrollerFL1404.jl")
include("../FSM/FSAM_old.jl")
include("FlightData.jl")

# Initialize aircraft model
GTM = Aircraft_t()
InitAircraft!(GTM,"../Aircraft/PARAMS/GTM.dat")

# Initialize controllers
Pilot = Controller_t()
InitController!(Pilot,"../Controller/PARAMS/PilotParams.txt")

EA    = Controller_t()
InitController!(EA,"../Controller/PARAMS/EAParams.txt")

# Initialize FSAM state machine
fsam  = FSM_t()
InitFSAM!(fsam,"../FSM/FSAMParams.txt")

# Declare and Initialize all variables used in the simulation
r2d    = 180/π
d2r    = π/180

Tfinal = 40
dt     = 0.01
tspan  = collect(0:dt:Tfinal)
N      = size(tspan,1)
tstop  = N
X      = zeros(N,16)
U      = zeros(N,7)
Modes  = zeros(N,2)
States = zeros(N,2)


x      = zeros(16)
x[1]   = 0.5
X[1,:] = x[:]
U0     = [0,0,0,0]

fsam.state1 = 1;
fsam.state2 = 1;

U[1,:]
# Simulate dynamics
for i=1:N-1

    x[:]           = X[i,:]

    (u,mode,state) = FSAM(GTM,x,Pilot,EA,
                          PilotControl,EAControl,fsam)

    if (x[13] < 0.5) && (u[4] < 0.1)
        tstop = i
        break;
    end

    Modes[i,:]     = mode[:]
    States[i,:]    = state[:]
    #u = [0.0,0.0,0.0,1.0,0.0,0.0,0.0]

    Wind = CrossWind[i,:][:]*0.1

    CheckSaturation(u)

    # Discrete inputs (1. Landing gear, 2. Engines operative )
    ud      = [1,2]
    dX      = ACDynamics(GTM,x,u,ud,Wind)
    X[i+1,:]       = x[:] + dX[:]*dt
    U[i,:]         = u[:]
    U[i+1,:]       = u[:] # Just to fill up the last entry
    Modes[i+1,:]   = Modes[i,:]
    States[i+1,:]  = States[i,:]
          
end

# Threshold values ( see parameter files for the values)
HeadingT1 = ones(tspan)*7
HeadingT2 = ones(tspan)*15
YT1       = ones(tspan)*10
YT2       = ones(tspan)*25

# Plot states and controls
figure(1)
subplot(511)
plot(tspan[1:tstop],X[1:tstop,13])
ylabel("Airspeed (m/s)")
subplot(512)
plot(tspan[200:tstop],X[200:tstop,12]+1)
ylabel("Altitude (m)")
subplot(513)
plot(tspan[1:tstop],X[1:tstop,8]*r2d)
ylabel("Pitch(deg)")
subplot(514)
plot(tspan[1:tstop],X[1:tstop,11])
ylabel("Y (m)")
subplot(515)
plot(tspan[1:tstop],X[1:tstop,9]*r2d)
ylabel("Yaw (deg)")

figure(2)
subplot(411)
plot(tspan[1:tstop],U[1:tstop,1]*r2d)
ylabel("ele (deg)")
subplot(412)
plot(tspan[1:tstop],U[1:tstop,2]*r2d)
ylabel("ail (deg)")
subplot(413)
plot(tspan[1:tstop],U[1:tstop,3]*r2d)
ylabel("rud (deg)")
subplot(414)
plot(tspan[1:tstop],U[1:tstop,4])
ylabel("thrt ")

#Plot FSAM outputs
figure(3)
subplot(411)
plot(tspan[1:tstop],Modes[1:tstop,1])
ylabel("FSM1 mode")
subplot(412)
plot(tspan[1:tstop],States[1:tstop,1])
ylabel("FSM1 state")
subplot(413)
plot(tspan[1:tstop],Modes[1:tstop,2])
ylabel("FSM2 mode")
subplot(414)
plot(tspan[1:tstop],States[1:tstop,2])
ylabel("FSM2 state")

figure(4)
subplot(321)
plot(tspan[1:tstop],X[1:tstop,13],linewidth="2")
ylabel("Airspeed (m/s)",fontsize=12)
xticks(fontsize=10)
yticks(fontsize=10)
xlim([0,40])
grid("on")
subplot(322)
plot(tspan[1:tstop],X[1:tstop,9]*r2d,linewidth="2")
plot(tspan,HeadingT1,linestyle="--","r",linewidth="2")
plot(tspan,HeadingT2,linestyle="--","m",linewidth="2")
plot(tspan,-HeadingT1,linestyle="--","r",linewidth="2")
plot(tspan,-HeadingT2,linestyle="--","m",linewidth="2")
text(20,7,"Inner threshold",fontsize=10)
text(20,13,"Outer threshold",fontsize=10)
ylabel("Heading (deg)",fontsize=12)
xticks(fontsize=10)
yticks(fontsize=10)
grid("on")
xlim([0,40])
subplot(325)
plot(tspan[1:tstop],States[1:tstop,2],linewidth="2")
ylabel("FSAM States",fontsize=12)
yticks([2,4,6,8,10,12,14],[2,4,6,8,10,12,14])
xticks(fontsize=10)
yticks(fontsize=10)
grid("on")
xlim([0,40])
subplot(324)
plot(tspan[1:956],U[1:956,3]*r2d,"r",label="Pilot",linewidth="2")
plot(tspan[957:end],U[957:end,3]*r2d,"b",label="EA",linewidth="2")
ylabel("Rudder (deg)",fontsize=12)
legend(loc="upper right",labelspacing=0.1)
xticks(fontsize=10)
yticks(fontsize=10)
grid("on")
xlim([0,40])
subplot(323)
plot(tspan[1:tstop],X[1:tstop,11],linewidth="2")
plot(tspan,YT1,linestyle="--","r",linewidth="2")
plot(tspan,YT2,linestyle="--","m",linewidth="2")
plot(tspan,-YT1,linestyle="--","r",linewidth="2")
plot(tspan,-YT2,linestyle="--","m",linewidth="2")
text(20,10,"Inner threshold",fontsize=10)
text(20,22,"Outer threshold",fontsize=10)
ylabel("Y (m)",fontsize=12)
xticks(fontsize=10)
yticks(fontsize=10)
grid("on")
xlim([0,40])
subplot(326)
plot(tspan[1:tstop],CrossWind[1:tstop,2]*0.1*1.94,linewidth="2")
ylabel("Crosswind (Knots)",fontsize=12)
xticks(fontsize=10)
yticks(fontsize=10)
grid("on")
xlim([0,40])
