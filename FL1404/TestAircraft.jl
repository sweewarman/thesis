using PyPlot

include("../Aircraft/Aircraft.jl")
include("../Controller/Controller.jl")
include("../FSM/FSAM.jl")

# Initialize aircraft model
GTM = Aircraft_t()
InitAircraft!(GTM,"../Aircraft/PARAMS/GTM.dat")

# Initialize controllers
Pilot = Controller_t()
InitController!(Pilot,"../Controller/PARAMS/PilotParams.txt")

EA    = Controller_t()
InitController!(EA,"../Controller/PARAMS/EAParams.txt")

# Initialize FSAM state machine
fsam  = FSM_t()
InitFSAM!(fsam,"../FSM/FSAMParams.txt")

r2d   = 180/π

Tfinal = 15
dt     = 0.01
tspan  = collect(0:dt:Tfinal)
N      = size(tspan,1)
X      = zeros(N,16)
U      = zeros(N,7)
Modes  = zeros(N,2)
States = zeros(N,2)

x      = zeros(16)
x[1]   = 0.5
X[1,:] = x[:]
U0     = [0,0,0,0]

fsam.state1 = 1;
fsam.state2 = 1;

for i=1:N-1

    x[:]           = X[i,:]
    (u,mode,state) = FSAM(GTM,x,Pilot,EA,
                          PilotControl,EAControl,fsam)

    Modes[i,:]     = mode[:]
    States[i,:]    = state[:]
    #u = [0.0,0.0,0.0,1.0,0.0,0.0,0.0]

    Wind = [0,0,0]

    # Discrete inputs (1. Landing gear, 2. Engines operative )
    ud      = [1,2]
    dX      = ACDynamics(GTM,x,u,ud,Wind)
    X[i+1,:]       = x[:] + dX[:]*dt
    U[i,:]         = u[:]
    U[i+1,:]       = u[:] # Just to fill up the last entry
end


# Plot states and controls
figure(1)
subplot(411)
plot(tspan,X[:,13])
ylabel("Airspeed (m/s)")
subplot(412)
plot(tspan,X[:,12])
ylabel("Altitude (m)")
subplot(413)
plot(tspan,X[:,8]*r2d)
ylabel("Pitch(deg)")
subplot(414)
plot(tspan,X[:,11])
ylabel("Y (m)")

figure(2)
subplot(411)
plot(tspan,U[:,1]*r2d)
ylabel("ele (deg)")
subplot(412)
plot(tspan,U[:,2]*r2d)
ylabel("ail (deg)")
subplot(413)
plot(tspan,U[:,3]*r2d)
ylabel("rud (deg)")
subplot(414)
plot(tspan,U[:,4])
ylabel("thrt ")


