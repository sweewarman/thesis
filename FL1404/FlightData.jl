# Script extracts FL1404 data for simulation

# 1st column is time, second column is wind speed in m/s
Data = readcsv("CrossWind.csv")

N     = size(Data,1)
Nspan = 4001
dt    = 0.01
CrossWind  = zeros(Nspan,3)

counter = 1

for i=1:Nspan
    time = i*dt

    if(counter < N)
        if time >= Data[counter+1,1]
            counter = counter + 1
        end

        CrossWind[i,2] = Data[counter,2]
    else
        CrossWind[i,2] = CrossWind[i-1,2]
    end
    
end

# Use a moving average filter to smooth cross-wind data
window = 100

for i=1:Nspan-window
    CrossWind[i,2] = sum(CrossWind[i:i+window,2])/window
end
