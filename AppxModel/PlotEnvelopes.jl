using PyPlot
using LsqFit

# Model for the envelope curves
model(X,w) = w[1] + w[2].*X + w[3].*X.^2 + w[4]*X.^3

function plot_envelope(Parameters,X;partitions=0,vecfield=0,plotgrid=true)
  

    Tmax    = Parameters[1,2]
    gamma   = Parameters[2,2]
    RLength = Parameters[3,2]
    M       = Parameters[4,2]
    Sref    = Parameters[5,2]
    hobs    = Parameters[6,2]
    CLmax   = Parameters[10,2]
    V2      = Parameters[16,2]
    Vw      = Parameters[17,2]

    g       = 9.8
    ρ       = 1.223
    W       = M*g
    
    Vstall  = sqrt(2*W/(ρ * Sref * CLmax));
    Vlof    = 1.02*Vstall;

   
    V1      = X[1] 
    V2p     = X[2] 
    Vef0    = X[3] 
    Vab0    = X[4]
    T       = X[5]

    param_input = vcat(Parameters,["T" T;])

    AB      = AnalyticalModel(param_input)

    A10 = AB[1];
    B10 = AB[2];
    A20 = AB[3];
    B20 = AB[4];
    A30 = AB[5];
    B30 = AB[6];
    A40 = AB[7];
    B40 = AB[8];
    A50 = AB[9];
    B50 = AB[10];
    
    Va1 = collect(linspace(0,V1,100));
    Va2 = collect(linspace(V1,Vlof,100));
    Va3 = collect(linspace(Vlof,V2p,100));
    Va4 = collect(linspace(0,Vab0,100));
    Va5 = collect(linspace(Vef0,V1,100));

    Sa1 = zeros(size(Va1,1));
    Sa2 = zeros(size(Va2,1));
    Sa3 = zeros(size(Va3,1));
    Sa4 = zeros(size(Va4,1));
    Sa5 = zeros(size(Va5,1));

    Int1 = V->Integrand(V,A10,B10,Vw,0)
    Int3 = V->Integrand(V,A30,B30,Vw,gamma*π/180)
    Int4 = V->Integrand(V,A40,B40,Vw,0)
    Int5 = V->Integrand(V,A20,B20,Vw,0)
    Int6 = V->Integrand(V,A40,B40,Vw,0)
    Int7 = V->Integrand(V,A20,B20,Vw,0)

    # Distance from V=0 to V=V1

    for ii=1:size(Va1,1)
        Sa1[ii] = quadgk(Int1,-Vw,Va1[ii])[1]
    end
 
    for i=1:size(Va2,1)
        Sa2[i] = Sa1[end] + quadgk(Int7,V1,Va2[i])[1]
    end

    # Distance from V=Vlof to V=V2
    for i =1:size(Va3,1)
        Sa3[i] = Sa2[end] + quadgk(Int3,Vlof,Va3[i])[1]
    end
    SV2 = Sa2[end] + quadgk(Int3,Vlof,V2)[1]

    # Distance from V = Vab0 to V = 0 (Tricky!!!)
    for i =1:size(Va4,1)
        Sa4[i] = quadgk(Int6,Vab0,Va4[i])[1]
    end
    # Distance travelled from V=Vef0 to V1
    for i =1:size(Va5,1)
        Sa5[i] =  quadgk(Int5,Vef0,Va5[i])[1]
    end
    # x and y limits for plotting
    xlimit = V2 + 10;
    ylimit = RLength + 100;

    # Runway 
    RunwayX =  collect(range(0,round(Int,V2) + 10));
    RunwayY =  ones(size(RunwayX,1)) * RLength;

    # Partitions of the envelop
    p_x1 = Vef0;
    p_x2 = V1;
    p_x3 = V2p;
    p_x4 = Vab0;
    p_x5 = Vlof;
    p_y1 = Sa1[end];
    p_y2 = RLength;
		 
    Fig_envelop = figure()
    plot(Va1,Sa1,color="blue");
    plot(Va2,Sa2,color="blue");
    plot(Va3,Sa3,color="blue");
    plot(Va4,Sa4,color="blue");
    plot(Va5,Sa5,color="blue");
    plot(RunwayX,RunwayY,color="red");

    scatter(V2p,RLength)
    scatter(V1,Sa1[end])
    scatter(Vef0,0)
    scatter(Vab0,0)

    #=
    if partitions
        plot(p_x1*ones(int(RLength)),range(0,int(RLength)),color="black");
        plot(p_x2*ones(int(RLength)+100),range(0,int(RLength)+100),color="black");
        plot(p_x3*ones(int(RLength)+100),range(0,int(RLength)+100),color="black");
        plot(collect(range(0,int(p_x3+1))),p_y1*ones(int(p_x3+1)),color="black");
    end
    =#

    xlim(0,xlimit);
    ylim(0,ylimit);
    
    if plotgrid
        grid("on")
    end
        
    xlabel("Airspeed (m/s)")
    ylabel("Distance (m)")


    fit1 = curve_fit(model,Va1,Sa1,[0.1,0.2,0.12,0.21])
    fit2 = curve_fit(model,Va4,Sa4,[0.1,0.2,0.12,0.21])
    fit3 = curve_fit(model,Va5,Sa5,[0.1,0.2,0.12,0.21])

    return Fig_envelop,fit1,fit2,fit3
end
