include("AnalyticalModel.jl")

# Integrand that provides displacement    
Integrand = (V,A,B,Vw,gamma)->((V + Vw)*cos(gamma))/(A - B*V^2)
      
function FuncRTO(x::Vector,grad::Vector)

    Vab0    = x[1]
    
    Int6 = V->Integrand(V,A40,B40,Vw,0)
    
    S    = quadgk(Int6,Vab0,Vw)[1]

    return abs(S - RLength)
            
end

function FuncEF(x::Vector,grad::Vector,funtype)

    
    Vef0    = x[1]

    Int3 = V->Integrand(V,A30,B30,Vw,gamma*pi/180)
    Int5 = V->Integrand(V,A20,B20,Vw,0)
    
    S5=quadgk(Int5,Vef0,Vlof)[1]
    S3=quadgk(Int3,Vlof,V2)[1]
    
    if funtype == 1
        return Vef0
    elseif funtype == 2
        return S3 + S5 - RLength
    elseif funtype == 3
        return hobs - S3*tan(gamma * pi/180)
    end
    
end

function FuncEFRTO(x::Vector,grad::Vector,funtype)

    
    V1      = x[1]

    Int6 = V->Integrand(V,A40,B40,Vw,0)
    Int5 = V->Integrand(V,A20,B20,Vw,0)
    
    S1=quadgk(Int5,Vef0,V1)[1]
    S2=quadgk(Int6,Vab0,V1)[1]
    
    if funtype == 1
        return V1
    elseif funtype == 2
        return S1 - S2
    end
    
end

function FuncTOGA(x::Vector,grad::Vector,funtype)

    T = x[1]
    parameters[18,2] = T

    # Compute analytical model based on given parameters
    AB      = AnalyticalModel(parameters)
 
    A10 = AB[1];
    B10 = AB[2];
    A40 = AB[7];
    B40 = AB[8];

    Int1 = V->Integrand(V,A10,B10,Vw,0)
    Int6 = V->Integrand(V,A40,B40,Vw,0)

    S1   = quadgk(Int1,Vw,V1)[1]
    S2   = quadgk(Int6,Vab0,V1)[1]

    if funtype == 1
        return T
    elseif funtype == 2
        return S1 - S2
    end


end


