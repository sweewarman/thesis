using NLopt

include("TK_Constraints.jl")
include("PlotEnvelopes.jl")

# Read parameters from file
parameters = readdlm("PARAMS/GTM_appxmdl.dat")

Tmax    = parameters[1,2]
gamma   = parameters[2,2]
RLength = parameters[3,2]
M       = parameters[4,2]
Sref    = parameters[5,2]
hobs    = parameters[6,2]
CLmax   = parameters[10,2]
V2      = parameters[16,2]
Vw      = parameters[17,2]
ρ       = 1.224
W       = M*9.8
    
Vstall  = sqrt(2*W/(ρ * Sref * CLmax));
Vlof    = 1.02*Vstall;

# Compute analytical model based on given parameters
AB      = AnalyticalModel(parameters)
 
A10 = AB[1];
B10 = AB[2];
A20 = AB[3];
B20 = AB[4];
A30 = AB[5];
B30 = AB[6];
A40 = AB[7];
B40 = AB[8];
A50 = AB[9];
B50 = AB[10];

# First find Vab0    
opt1    = Opt(:LN_COBYLA,1)
xtol_rel!(opt1,1e-4)
F1 = (x,g)->FuncRTO(x,g)
min_objective!(opt1,F1)
 
lower_bounds!(opt1,[50])
upper_bounds!(opt1,[100])

(minf1,minx1,res1) = optimize(opt1,[90.])

Vab0 = minx1[1]

@printf("First optimization\n Status = %s, Vab0 = %f\n",res1,Vab0)

# Lets find Vef0
opt2    = Opt(:LN_COBYLA,1)
xtol_rel!(opt2,1e-8)
F2 = (x,g)->FuncEF(x,g,1)
min_objective!(opt2,F2)

equality_constraint!(opt2,   (x,g)->FuncEF(x,g,2),1e-8)
inequality_constraint!(opt2, (x,g)->FuncEF(x,g,3),1e-8)

lower_bounds!(opt2,[0.0])
upper_bounds!(opt2,[Vab0])

(minf2,minx2,res2) = optimize(opt2,[50.])

Vef0 = minx2[1]

@printf("Second optimization\n Status = %s, Vef0 = %f\n",res2,Vef0)

# Lets find V1
opt3    = Opt(:LN_COBYLA,1)
xtol_rel!(opt3,1e-8)
F3 = (x,g)->FuncEFRTO(x,g,1)
min_objective!(opt3,F3)

equality_constraint!(opt3,   (x,g)->FuncEFRTO(x,g,2),1e-8)

lower_bounds!(opt3,[Vef0])
upper_bounds!(opt3,[Vlof])

(minf3,minx3,res3) = optimize(opt3,[Vef0+0.4])

V1 = minx3[1]

@printf("Third optimization\n Status = %s, V1 = %f\n",res3,V1)

# Lets find T 
opt4    = Opt(:LN_COBYLA,1)
xtol_rel!(opt4,1e-8)
F4 = (x,g)->FuncTOGA(x,g,1)
min_objective!(opt4,F4)

equality_constraint!(opt4,   (x,g)->FuncTOGA(x,g,2),1e-8)

lower_bounds!(opt4,[2*W*sind(gamma)])
upper_bounds!(opt4,[Tmax])

(minf4,minx4,res4) = optimize(opt4,[Tmax*0.8])

T = minx4[1]

@printf("Fourth optimization\n Status = %s, T = %f\n",res4,T)

Xsol = [V1,V2,Vef0,Vab0,T]

(fig1,fit1,fit2,fit3) = plot_envelope(parameters,Xsol)

writedlm("PARAMS/GTM_VXenv.dat",hcat(fit1.param,fit2.param,fit3.param))
