using PyPlot
using Cubature

# Integrand that provides displacement    
Integrand = (V,A,B,Vw,gamma)->((V + Vw)*cos(gamma))/(A - B*V^2)
# Read parameters from file
parameters = readdlm("PARAMS/FL407.dat")

include("AnalyticalModel.jl")

Tmax    = parameters[1,2]
gamma   = parameters[2,2]
RLength = parameters[3,2]
M       = parameters[4,2]
Sref    = parameters[5,2]
hobs    = parameters[6,2]
CLmax   = parameters[10,2]
V2      = parameters[16,2]
Vw      = parameters[17,2]
g       = 9.8
ρ       = 1.223
W       = M*g

parameters[18,2] = Tmax

Vstall  = sqrt(2*W/(ρ * Sref * CLmax));
Vlof    = 1.02*Vstall;


# Compute analytical model based on given parameters
AB      = AnalyticalModel(parameters)
 
A10 = AB[1];
B10 = AB[2];
A20 = AB[3];
B20 = AB[4];
A30 = AB[5];
B30 = AB[6];
A40 = AB[7];
B40 = AB[8];
A50 = AB[9];
B50 = AB[10];

Int2   = V->Integrand(V,A30,B30,Vw,gamma*pi/180)
Int1   = V->Integrand(V,A20,B20,Vw,0)
    
N      = 1000*60
Vdata1 = zeros(1,N)
Xdata1 = zeros(1,N)

Vdata2 = zeros(1,N)
Xdata2 = zeros(1,N)


Vrunway  = collect(linspace(0,100,100))
Xrunway  = ones(1,100)*3500

deltaT   = 0.001


Vef0      = 45
Vdata1[1] = Vef0
Vdata2[1] = Vef0

count = 0;

for i=1:length(Vdata1)-1

	if Vdata1[i] >= Vlof		
		Vdata1[i+1] = Vdata1[i] + (A30 - B30*Vdata1[i]^2)*deltaT*cosd(gamma)
		Xdata1[i+1] = Xdata1[i] + Vdata1[i]*deltaT 
		Xdata2[i]   = Xdata2[count] + hquadrature(Int2,Vlof,Vdata1[i])[1]
		Xdata2[i+1] = Xdata2[i]
	else
		Vdata1[i+1] = Vdata1[i] + (A20 - B20*Vdata1[i]^2)*deltaT
		Xdata1[i+1] = Xdata1[i] + Vdata1[i]*deltaT 
		Xdata2[i]   = hquadrature(Int1,Vef0,Vdata1[i])[1]
		Xdata2[i+1] = Xdata2[i];
		count       = count + 1;
	end
end

N2      = 1000*65
Vdata3 = zeros(1,N2)
Xdata3 = zeros(1,N2)

Vab0   = 92.5
Vdata3[1] = Vab0

for i=1:length(Vdata3)-1

	Vdata3[i+1] = Vdata3[i] + (A40 - B40*Vdata3[i]^2)*deltaT
	Xdata3[i+1] = Xdata3[i] + Vdata3[i]*deltaT 
			
end

N3      = 1000*65
Vdata4 = zeros(1,N3)
Xdata4 = zeros(1,N3)

for i=1:length(Vdata4)-1

	if Vdata4[i] >= Vlof		
		Vdata4[i+1] = Vdata4[i] + (A50 - B50*Vdata4[i]^2)*deltaT*cosd(gamma)
		Xdata4[i+1] = Xdata4[i] + Vdata4[i]*deltaT 
	else
		Vdata4[i+1] = Vdata4[i] + (A10 - B10*Vdata4[i]^2)*deltaT
		Xdata4[i+1] = Xdata4[i] + Vdata4[i]*deltaT 
	end
			
end

figure(1)
color = "r"
plot(Vdata1',Xdata1',color)
plot(Vdata3',Xdata3',color)
plot(Vrunway,Xrunway',color)
xlim([0,100])
plot(Vdata4',Xdata4',color)


