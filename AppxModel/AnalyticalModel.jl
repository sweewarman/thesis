function AnalyticalModel(Parameters)
    
    Tmax    = Parameters[1,2]
    gamma   = Parameters[2,2]
    RLength = Parameters[3,2]
    M       = Parameters[4,2]
    S       = Parameters[5,2]
    hobs    = Parameters[6,2]
    mu      = Parameters[7,2]
    mub     = Parameters[8,2]
    CL      = Parameters[9,2]
    CLmax   = Parameters[10,2]
    CD      = Parameters[11,2]
    rho     = Parameters[12,2]
    alpha   = Parameters[13,2]
    T       = Parameters[18,2]
    Neng    = Parameters[19,2]
    g       = 9.8
    W       = M*g
    
    if Neng <= 2
        Tredfac = 0.5
    else
        Tredfac = 0.75
    end

    # Takeoff with TOGA (Tmax) thrust setting
    A10 = g * ( T / (W ) - mu );
    B10 = g/W * ( 0.5 * rho * S * ( CD - mu * CL ) );
    
    # Continued takeoff after single  engine failure (twin engine aircraft)
    A20 = g * ( Tredfac * Tmax / (W ) - mu );
    B20 = B10;
    
    # Lift off after continued takeoff with single engine failure
    A30 = g * ( Tredfac * Tmax / (W) - sin( gamma * pi/180 ));
    B30 = g/W * ( 0.5 * rho * S *  CD * 0.75 ); 

    # Deceleration after rejected takeoff
    A40 = g * ( 0 / (W ) - mub );
    B40 = g/W * ( 0.5 * rho * S * ( CD - mub * CL ) );

    # Lift off after continued takeoff
    A50 = g * ( T / (W ) - sin( gamma * pi/180 ));
    B50 = g/W * ( 0.5 * rho * S *  CD ); 

    return [A10,B10,A20,B20,A30,B30,A40,B40,A50,B50]
end
