AppxModel
=========

This folder contains script to compute takeoff (V-x) envelopes based on approximate takeoff dynamics model. Approximate model's parameters are specified in a file inside PARAMS/. See ComputeVX_Env.jl for an example.