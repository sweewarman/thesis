#===================================================================
This file defines the main Moore machine engine which receives 
aircraft states as inputs, determins the alphabet symbols and then
transitions between states. Output is the current mode

Note: The transitions in this file are defined according to the old
FSM formulation (version before model checking). This contains bugs 
that can result in tail-strikes.
=====================================================================#

include("FSAM.jl")


function FSAM_FSM(fsm::FSM_t,x::Array{Float64,1},
                  U1::Array{Float64,1},U2::Array{Float64,1})

    r2d = 180/π
    p = x[4]*r2d
    q = x[5]*r2d
    r = x[6]*r2d
    ϕ = x[7]*r2d
    θ = x[8]*r2d
    ψ = x[9]*r2d
    d = x[10]
    y = abs(x[11])
    h = x[12]
    V = x[13]

    fsm_alp = Alphabet_t()
    GetAlphabet!(fsm_alp,fsm,x,U1,U2)

    mode1,mode2 = -1,-1        

    # Rest
    if fsm.state1 == 1
        mode1 = 1
        if fsm_alp.V0
            fsm.state1 = 2
        end
    # 0 < V < Vmcg
    elseif fsm.state1 == 2 
        mode1 = 1
        if fsm_alp.Vmcg 
            fsm.state1 = 3
        end
    # Vmcg <= V <= V1
    elseif fsm.state1 == 3 
        mode1 = 1
        if fsm_alp.V1
            fsm.state1 = 4
        end

    # V1 <= V < VR
    elseif fsm.state1 == 4 
        mode1 = 1
        if fsm_alp.VR
            fsm.state1 = 5
        end
            
        if fsm_alp.pitch_PR1 
            fsm.state1 = 9
        end

    # VR <= V < Vlof
    elseif fsm.state1 == 5 
        mode1 = 1
            
        if fsm_alp.pitch_TS1
            fsm.state1 = 10
        elseif fsm_alp.Vlof
            fsm.state1 = 6
        end

        # Vlof <= V < V2
    elseif fsm.state1 == 6 
        mode1 = 1
            
        if fsm_alp.pitch_TS1
            fsm.state1 = 6
        elseif fsm_alp.V2 
            fsm.state1 = 7
        end

        
    # V2 <= V < Vfp
    elseif fsm.state1 == 7 
        mode1 = 1
        if fsm_alp.pitch_TS1
            fsm.state1 = 7
        elseif fsm_alp.Vfp
            fsm.state1 = 7
        end
                        
    elseif fsm.state1 == 9 
        mode1 = 2
        
        if fsm_alp.VR 
            fsm.state1 = 5
        end

    elseif fsm.state1 == 10 
        mode1 = 2
    
        if fsm_alp.Vlof 
            fsm.state1 = 6
            if fsm_alp.pitch_TS1 
                fsm.state1 = 11
            end
        end

    elseif fsm.state1 == 11 
        mode1 = 2

        if !fsm_alp.pitch_TS1
            fsm.state1 = 6
        elseif fsm_alp.V2 
            fsm.state1 = 12
        end
    elseif fsm.state1 == 12
        mode1 = 2

        if !fsm_alp.pitch_TS1 
            fsm.state1 = 7
        end
    end
    #=========================================================#
    # Lateral state machine
        
    # Rest
    if fsm.state2 == 1 
        mode2 = 1
        if fsm_alp.V0 
            fsm.state2 = 2
        end
        # 0 < V < Vmcg
    elseif fsm.state2 == 2 
        mode2 = 1

        if fsm_alp.Vmcg 
            fsm.state2 = 3
        end

        if fsm_alp.d1 
            fsm.state2 = 8
        end

        # Vmcg <= V <= V1
    elseif fsm.state2 == 3 
        mode2 = 1
            
        if fsm_alp.V1 
            fsm.state2 = 4
        end

        if fsm_alp.d1 
            fsm.state2 = 9
        end
        # V1 <= V < VR
    elseif fsm.state2 == 4 
        mode2 = 1
            
        if fsm_alp.VR 
            fsm.state2 = 5
        end
        if fsm_alp.d1 
            fsm.state2 = 10
        end
        # VR <= V < Vlof
    elseif fsm.state2 == 5 
        mode2 = 1

        if fsm_alp.Vlof 
                fsm.state2 = 6
        end
        if fsm_alp.d1 
            fsm.state2 = 11
        end
        # Vlof <= V < V2
    elseif fsm.state2 == 6 
        mode2 = 1

        if fsm_alp.V2 
            fsm.state2 = 7
        end

        if fsm_alp.d1 
            fsm.state2 = 12
        end
        # V2 <= V < Vfp
    elseif fsm.state2 == 7 
        mode2 = 1

        if fsm_alp.Vfp 
                fsm.state2 = 7
        end

        if fsm_alp.d1 
            fsm.state2 = 13
        end
    elseif fsm.state2 == 8 
        mode2 = 2

        if fsm_alp.Vmcg 
            fsm.state2 = 9
        end

        if fsm_alp.dd1
            fsm.state2 = 14
        end
    elseif fsm.state2 == 9 
        mode2 = 2
        
        if fsm_alp.V1 
            fsm.state2 = 10
        end

        if fsm_alp.dd1
            fsm.state2 = 14
        end
    elseif fsm.state2 == 10 
        mode2 = 2

        if fsm_alp.VR 
            fsm.state2 = 11
        end
    elseif fsm.state2 == 11 
        mode2 = 2
        
        if fsm_alp.Vlof 
            fsm.state2 = 12
        end
    elseif fsm.state2 == 12 
        mode2 = 2
        
        if fsm_alp.V2 
            fsm.state2 = 13
        end
        
        if fsm_alp.d1
            fsm.state2 = 13
        end

        
    elseif fsm.state2 == 13 
        mode2 = 2

        if fsm_alp.Vfp 
            fsm.state2 = 13
        end

    elseif fsm.state2 == 14

        mode2 = 3
        
    end
    return (mode1,mode2)

end
