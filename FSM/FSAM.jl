#====================================================================
This file defines the following:
1. Data structure that will hold the parameters
   of the finite state machine.
2. Data structure for the alphabet symbols.
3. Initialization functions
4. Switching function that selects the appropriate control authority
   based on fsam state
====================================================================#


# Data structure for FSM parameters
type FSM_t

    state1 # State of longitudinal FSM
    state2 # State of lateral FSM
    Vmcg   # Minimum ground control speed
    V1     # V1 decision speed
    VR     # Rotation speed
    Vlof   # Lift off speed
    V2     # Takeoff safety speed
    Vfp    # Flap retraction speed
    p1     # Premature rotation threshold
    p2     # Tail strike threshold
    h1     # Max altitude until which tail strike is possible
    y1     # Cross track deviation constraint - inner
    y2     # Cross track deviation constraint - outer
    d1     # Heading deviation constraint - inner
    d2     # Heading deviation constraint - outer

    # Inner constructor
    FSM_t() = new() 
end

# Data structures for FSM alphabets
# See JAIS 2015 Journal paper for description of each alphabet
type Alphabet_t
    
    # Alphabet symbols a set to true if the corresponding conditions
    # are satisfied
    V0::Bool
    Vmcg::Bool
    V1::Bool
    VR::Bool
    Vlof::Bool
    V2::Bool
    Vfp::Bool

    pitch_PR1::Bool
    pitch_TS1::Bool
    
    pitch_PR2::Bool
    pitch_TS2::Bool

    d1::Bool
    d2::Bool

    dd1::Bool
    dd2::Bool

    F::Bool

    # Inner constrcutor
    Alphabet_t() = new()
end

# Function to initalize the FSM datastructure
function InitFSAM!(fsm::FSM_t,filename::ASCIIString)
    
    File = open(filename)

    fsm.state1 = 1
    fsm.state2 = 1
    fsm.Vmcg  = parse(Float64,split(readline(File))[2])
    fsm.V1    = parse(Float64,split(readline(File))[2])
    fsm.VR    = parse(Float64,split(readline(File))[2])
    fsm.Vlof  = parse(Float64,split(readline(File))[2])
    fsm.V2    = parse(Float64,split(readline(File))[2])
    fsm.Vfp   = parse(Float64,split(readline(File))[2])
    fsm.p1    = parse(Float64,split(readline(File))[2])
    fsm.p2    = parse(Float64,split(readline(File))[2])
    fsm.h1    = parse(Float64,split(readline(File))[2])
    fsm.y1    = parse(Float64,split(readline(File))[2])
    fsm.y2    = parse(Float64,split(readline(File))[2])
    fsm.d1    = parse(Float64,split(readline(File))[2])
    fsm.d2    = parse(Float64,split(readline(File))[2])

    close(File)
end

# V-X Envelope parametrization
VXmodel(X,params) = params[1] + params[2].*X + params[3].*X.^2 + 
                                       params[4].*X.^3

VXparams = readdlm("../AppxModel/PARAMS/GTM_VXenv.dat")


function GetAlphabet!(fsm_alp::Alphabet_t,fsm::FSM_t,
                     x::Array{Float64,1},
                     U1::Array{Float64,1},U2::Array{Float64,1})

    #================================================================
    # Function that determines the true/false state of the alphabet
    symbols
    ================================================================#

    r2d = 180/π
    p = x[4]*r2d
    q = x[5]*r2d
    r = x[6]*r2d
    ϕ = x[7]*r2d
    θ = x[8]*r2d
    ψ = x[9]*r2d
    d = x[10]
    y = abs(x[11])
    h = x[12]
    V = x[13]

    # Check for deviation from TOGA envelope
    x_env = VXmodel(V,VXparams[:,1])

    if(d > x_env)
        fsm_alp.F = true
    else
        fsm_alp.F = false
    end

    if V > 0
        fsm_alp.V0 = true
    else
        fsm_alp.V0 = 0
    end
    
    if V > fsm.Vmcg
        fsm_alp.Vmcg = true
    else
        fsm_alp.Vmcg = false
    end

    if V > fsm.V1
        fsm_alp.V1 = true
    else
        fsm_alp.V1 = false
    end
    
    if V > fsm.Vlof
        fsm_alp.Vlof = true
    else
        fsm_alp.Vlof = false
    end

    if V > fsm.VR
        fsm_alp.VR = true
    else
        fsm_alp.VR = false
    end

    if V > fsm.V2
        fsm_alp.V2 = true
    else
        fsm_alp.V2 = false
    end
 
    if V > fsm.Vfp
        fsm_alp.Vfp = true
    else
        fsm_alp.Vfp = false
    end
 
    if θ > fsm.p1 && h < fsm.h1 
        fsm_alp.pitch_PR1 = true
    else
        fsm_alp.pitch_PR1 = false
    end

    if θ > fsm.p2 && h < fsm.h1 
        fsm_alp.pitch_TS1 = true
    elseif θ > fsm.p2-7 && q > 5 && h < fsm.h1
        fsm_alp.pitch_TS1 = true
    else
        fsm_alp.pitch_TS1 = false
    end

    if abs(y) > fsm.y1 
        fsm_alp.d1 =  true
    elseif abs(ψ) > fsm.d1
        fsm_alp.d1 = true
    else
        fsm_alp.d1 = false
    end

    if abs(y) > fsm.y2 
        fsm_alp.dd1 =  true
    elseif abs(ψ) > fsm.d2
        fsm_alp.dd1 = true
    else
        fsm_alp.dd1 = false
    end 

end

# Function that selects the appropriate controller based on FSM 
# outputs
function FSAM(Aircraft,x,
              Pilot,EA,
              PilotControl,EAControl,fsm)

    U1 = PilotControl(Aircraft,x,Pilot)
    U2 = EAControl(Aircraft,x,EA)

    (mode1,mode2) = FSAM_FSM(fsm,x,U1,U2)
    
    
    if mode1 == 1 && mode2 == 1
        U = U1
    elseif mode1 == 2 && mode2 == 2
        U = U2
    elseif mode1 == 1 && mode2 == 2
        U    = [0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        U[1] = U1[1]
        U[2] = U2[2]
        U[3] = U2[3]
        U[4] = U2[4]
    elseif mode1 == 2 && mode2 == 1
        U    = [0.0,0.0,0.0,0.0,0.0,0.0,0.0]
        U[1] = U2[1]
        U[2] = U1[2]
        U[3] = U1[3]
        U[4] = U2[4]
    elseif (mode1 == 3) || (mode2 == 3)
        # Abort mode
        U    = [0.0,0.0,0.0,0.0,1.0,1.0,0.0]
        U[1] = 0
        U[2] = U2[2]
        U[3] = U2[3]
        U[4] = 0
        U[5] = 2
        U[6] = 2
    end
    
    try
        return (U,[mode1,mode2],[fsm.state1,fsm.state2])
    catch
        @printf("States=%d,%d\n",state1,state2)
        @printf("V = %f, X = %f, pitch = %f\n",
                x[13],x[10],x[8]*180/π)
    end
end
