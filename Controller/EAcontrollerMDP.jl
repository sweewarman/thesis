include("Controller.jl")

# V-X Envelope parametrization
VXmodel(X,params) = params[1] + params[2].*X + params[3].*X.^2 + 
                                       params[4].*X.^3

function EAControl(Aircaft::Aircraft_t,
                   x::Array{Float64,1},EA::Controller_t)

    d2r = π/180
    p = x[4]
    q = x[5]
    r = x[6]
    ϕ = x[7]
    θ = x[8]
    ψ = x[9]
    d = x[10]
    y = x[11]
    h = x[12]
    V = x[13]
    eng = x[17]

    #=============================================================#
    # Elevator input
    θ_ref   = EA.θ_ref*d2r
    θ_refTS = EA.θ_refTS*d2r
    kp      = EA.kpe
    kd      = EA.kde
    Vᵣ       = EA.Vᵣ
    θ_TS    = EA.θ_TS*d2r
    h_TS    = EA.h_TS

    if V > Vᵣ
        if (θ_TS - θ) < 0 && (h_TS - h) > 0
            Eδₑ     = kp * (θ_refTS - θ) + kd*q
        else
            Eδₑ     = kp * (θ_ref - θ) + kd*q
        end
    else
        Eδₑ     = kp * (0 - θ) + kd*q
    end

    #Aileron Inputs
    Eδₐ = 0

    # Rudder Inputs
    ψ_ref   = EA.kpy * y
    kp      = EA.kpr
    kd      = EA.kdr

    Eδᵣ      = kp * (ψ_ref - ψ) + kd*r

    ET = 1

    # Check for deviation from TOGA envelope
    x_env = VXmodel(V,VXparams[:,1])

    if eng > 1
        if (d > x_env) && (V<EA.V1)
            ET = 0
        elseif (d > x_env) && (V>EA.V1)
            ET = 1
        else
            ET = 1
        end
    elseif eng < 2
        if (d > x_env) && (V<EA.V1)
            ET = 0
        elseif (d > x_env) && (V>EA.V1)
            ET = 1
        elseif (d < x_env) && (V<EA.V1)
            ET = 0
        elseif (d < x_env) && (V>EA.V1)
            ET = 1
        else
            ET = 1
        end
    end

    if y>0
        BrakesL = 2.0
        BrakesR = 0.0
    elseif y<0
        BrakesR = 2.0
        BrakesL = 0.0
    else
        BrakesR = 0.0
        BrakesL = 0.0
    end

    if ET < 0.1
        BrakesL  = 3
        BrakesR  = 3
    end

    U2 = [Eδₑ,Eδₐ,Eδᵣ,ET,BrakesL,BrakesR,0.0]

    return U2
end
