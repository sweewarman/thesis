include("Controller.jl")

function PilotControl(Aircaft::Aircraft_t,
                      x::Array{Float64,1},Pilot::Controller_t)

    
    d2r = π/180
    p = x[4]
    q = x[5]
    r = x[6]
    ϕ = x[7]
    θ = x[8]
    ψ = x[9]
    d = x[10]
    y = x[11]
    h = x[12]
    V = x[13]
    eng = x[17]

    #=============================================================#
    # Elevator input
    θ_ref  = Pilot.θ_ref*d2r
    kp     = Pilot.kpe
    kd     = Pilot.kde
    Vᵣ     = Pilot.Vᵣ

    if V > Vᵣ
        Pδₑ     = kp * (θ_ref - θ) + kd*q
    else
        Pδₑ     = kp * (0 - θ)   + kd*q
    end

    #Aileron Inputs
    Pδₐ = 0

    # Rudder Inputs
    ψ_ref   = Pilot.kpy * y
    kp      = Pilot.kpr
    kd      = Pilot.kdr

    Pδᵣ      = kp * (ψ_ref - ψ) + kd*r

    if V < Pilot.V1
        if eng < 2
            PT       = 0
        else
            PT       = Pilot.Tref
        end
    else
        PT = Pilot.Tref
    end

    if y>0
        BrakesL = 2.0
        BrakesR = 0.0
    elseif y<0
        BrakesR = 2.0
        BrakesL = 0.0
    else
        BrakesR = 0.0
        BrakesL = 0.0
    end

    if PT < 0.1
        BrakesL  = 3
        BrakesR  = 3
    end


    U1 = [Pδₑ,Pδₐ,Pδᵣ,PT,BrakesL,BrakesR,0.0]

    return U1

end
