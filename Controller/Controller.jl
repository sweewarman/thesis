#=============================================================================
This file defines the data structure for a PD control law.
In most cases, the pilot and the envelope-aware controller are
represented as PD control laws. 
==============================================================================#

type Controller_t
    θ_ref::Float64     # Reference pitch attitude
    θ_refTS::Float64   # Reference pitch attitude when threshold is violated
    θ_TS::Float64      # Tail strike threshold for pitch
    h_TS::Float64      # Tail strike threshold for pitch 
    kpe::Float64       # Proportional gain - pitch
    kde::Float64       # Derivative gain - pitch
    Vᵣ::Float64         # Rotation speed

    Tmax::Float64      # Maximum thrust ∈ [0,1]
    dlimit::Float64    # Distance at V1 speed

    ψ_ref::Float64     # Heading reference command
    kpr::Float64       # Proportional gain - rudder
    kdr::Float64       # Derivative gain - rudder
    kpy::Float64       # Proportional gain - cross track error
    V1::Float64        # V1 decision speed
    ymax::Float64      # Max cross track deviation

    abort::Float64     # Abort flag

    Tref::Float64      # Reference thrust ∈ [0,1]

    # Inner constructor
    Controller_t() = new()
end

function InitController!(control::Controller_t,filename::ASCIIString)
    #================================================================
    Function to initialize controller parameters
    - Specify file name that contains the parameters
    ================================================================#

    File = open(filename)

    control.Vᵣ       = parse(Float64,split(readline(File))[2])
    control.θ_ref   = parse(Float64,split(readline(File))[2])
    control.θ_refTS = parse(Float64,split(readline(File))[2])
    control.θ_TS    = parse(Float64,split(readline(File))[2])
    control.h_TS    = parse(Float64,split(readline(File))[2])
    control.kpe     = parse(Float64,split(readline(File))[2])
    control.kde     = parse(Float64,split(readline(File))[2])
    control.Tmax    = parse(Float64,split(readline(File))[2])
    control.dlimit  = parse(Float64,split(readline(File))[2])
    control.kpr     = parse(Float64,split(readline(File))[2])
    control.kdr     = parse(Float64,split(readline(File))[2])
    control.kpy     = parse(Float64,split(readline(File))[2])
    control.V1      = parse(Float64,split(readline(File))[2])
    control.ymax    = parse(Float64,split(readline(File))[2])

    close(File)


end

function CheckSaturation(U::Array{Float64,1})
    #================================================================
    Function to impose saturation constraints on surface and throttle
    control inputs
    =================================================================#

    d2r = π/180


    if U[1] >= 25*d2r
        U[1] = 25*d2r;
    elseif U[1] <= -25*d2r
        U[1] = -25*d2r;
    end

    if U[2] >= 25*d2r
        U[2] = 25*d2r;
    elseif U[2] <= -25*d2r
        U[2] = -25*d2r;
    end

    if U[3] >= 25*d2r
        U[3] = 25*d2r;
    elseif U[3] <= -25*d2r
        U[3] = -25*d2r;
    end

    if U[4] >= 1
        U[4] = 1;
    elseif U[4] <= 0
        U[4] = 0;
    end

end
