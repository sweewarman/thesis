

function GearFM(Aircraft::Aircraft_t,x::Array{Float64,1},u::Array{Float64,1},wL,wR,wN,LG)
    #=
     Function that computes the forces and moments acting on the airframe due to the oleo struts.
    =#
    ub      = x[1]
    vb      = x[2]
    wb      = x[3]
    pb     = x[4]
    qb     = x[5]
    rb     = x[6]
    ϕ      = x[7]
    θ      = x[8]
    ψ      = x[9]

    Ze     = -x[12]

    dcm    = DCM(ϕ,θ,ψ)
    Vb     = [ub,vb,wb]
    Ve     = dcm*Vb

    Vac    = Ve[3]

    BrakeL = u[5]*Aircraft.BrakeT
    BrakeR = u[6]*Aircraft.BrakeT
    steer  = u[7]

    lxL,lyL,lzL = Aircraft.lxL,Aircraft.lyL,Aircraft.lzL
    lxR,lyR,lzR = Aircraft.lxR,Aircraft.lyR,Aircraft.lzR
    lxN,lyN,lzN = Aircraft.lxN,Aircraft.lyN,Aircraft.lzN

    k_L,k_R,k_N = Aircraft.k_L,Aircraft.k_R,Aircraft.k_N
    c_L,c_R,c_N = Aircraft.c_L,Aircraft.c_R,Aircraft.c_N


    Rwheel  = Aircraft.Tradius;
    Iwheel  = Aircraft.Tmass * (Rwheel)^2/2;

    #Oleo locations in the body frame
    rposL  = [ lxL, lyL, 0 ];
    rposR  = [ lxR, lyR, 0 ];
    rposN  = [ lxN, lyN, 0 ];

    #Oleo locations in the inertial frame
    oleoL  = dcm*rposL
    oleoR  = dcm*rposR
    oleoN  = dcm*rposN

    #Deflection rate of oleos in inertial frame due to attitude changes (using transport theorem)
    w_cross  = [0 -rb  qb;rb 0 -pb;-qb pb 0]
    rPos   = hcat(rposL,rposR,rposN)

    oleodotB = w_cross*rPos

    oleodot  = dcm*oleodotB

    #Total deflection of oleos - z direction
    oleoLz   = oleoL[3] + Ze;
    oleoRz   = oleoR[3] + Ze;
    oleoNz   = oleoN[3] + Ze;

    #Total deflection rate of oleos
    oleoLdot = oleodot[3,1]  + Vac;
    oleoRdot = oleodot[3,2]  + Vac;
    oleoNdot = oleodot[3,3]  + Vac;

    #Normal load on the gears
    Fsn = -k_N * oleoNz;
    Fdn = -c_N * oleoNdot;

    Fsl = -k_L * oleoLz;
    Fdl = -c_L * oleoLdot;

    Fsr = -k_R * oleoRz;
    Fdr = -c_R * oleoRdot;

    FzL = Fsl+Fdl;
    FzR = Fsr+Fdr;
    FzN = Fsn+Fdn;

    if FzL >= 0
        FzL=0
    end

    if FzR >= 0
        FzR=0
    end

    if FzN >= 0
        FzN=0
    end

    #=
    Use this snippet of code if you don't want to explicitly model
    the tire dynamics.

    #Coefficient of fricition related through the magic formula
    # Simplified by specifying a standard value for friction coeff
    muL   = Aircraft.mu
    muR   = Aircraft.mu
    muN   = Aircraft.mu

    #Longitudinal loads on the wheels
    FxL   = -muL*abs(FzL);
    FxR   = -muR*abs(FzR);
    FxN   = -muN*abs(FzN);
    =#

    #Lateral wheel slip
    if ub > 0
      #Wheel velocities
      UwR   = ub  - rb*lyR;
      VwR   = vb + rb*lxR;

      UwL   = ub - rb*lyL;
      VwL   = vb + rb*lxL;

      UwN   = ub;
      VwN   = vb + rb*lxN;

      #lateral Slip angle of the wheels
      alpL = atan(VwL/UwL);
      alpR = atan(VwR/UwR);
      alpN = -steer + atan(VwN/UwN);

      # Enforce condition to prevent wheel spinning faster
      # than translational velocity of airplane
      if abs(wL)*Rwheel > UwL
          wL = sign(wL) * UwL/Rwheel
      end
        
      if abs(wR)*Rwheel > UwR
          wR = sign(wR) * UwR/Rwheel
      end
        
      if abs(wN)*Rwheel > UwN
          wN = sign(wN) * UwN/Rwheel
      end 

      #longitudinal slip ratio for left and right wheels
      sigL = 1 - abs(wL)*Rwheel/UwL;
      sigR = 1 - abs(wR)*Rwheel/UwR;
      sigN = 1 - abs(wN)*Rwheel/UwN;

      if(sigL >  1)
          sigL= 1;
      end
      if(sigL < -1)
          sigL=-1;
      end

      if(sigR >  1)
          sigR= 1;
      end
      if(sigR < -1)
          sigR=-1;
      end

      if(sigN >  1)
          sigN= 1;
      end
      if(sigN < -1)
          sigN=-1;
      end

      #Majic formula parameters
      Bp    = 10;
      Cp    = 1.9;
      Dp    = 1;

      #Coefficient of fricition related through the magic formula
      muL   = Dp*sin(Cp*atan(Bp*sigL));
      muR   = Dp*sin(Cp*atan(Bp*sigR));
      muN   = Dp*sin(Cp*atan(Bp*sigN));

      #=
      Coefficient of fricition related through the magic formula
      Simplified by specifying a standard value for friction coeff
      This works better for rejected takeoff scenario (Current no differential
      braking for RTO)
      =#

      if u[4] < 0.1
          muL   = 0.2
          muR   = 0.2
          muN   = 0.02
      end

      #Longitudinal loads on the wheels
      FxL   = -muL*abs(FzL);
      FxR   = -muR*abs(FzR);
      FxN   = -muN*abs(FzN);

      FymaxL  = -7.39e-7 * FzL^2 + 5.11e-1*FzL
      FymaxR  = -7.39e-7 * FzR^2 + 5.11e-1*FzR
      FymaxN  = -3.53e-6 * FzN^2 + 8.33e-1*FzN

      alpoptN = 3.52e-9 *FzN^2 + 2.80e-5*FzN + 13.8
      alpoptL = 1.34e-10*FzL^2 + 1.06e-5*FzL + 6.72
      alpoptR = 1.34e-10*FzR^2 + 1.06e-5*FzR + 6.72

      #Lateral loads on the wheels
      FyL   = -(-2*(FymaxL * alpoptL*alpL)/(alpoptL^2 + alpL^2))
      FyR   = -(-2*(FymaxR * alpoptR*alpR)/(alpoptR^2 + alpR^2))
      FyN   = -(-2*(FymaxN * alpoptN*alpN)/(alpoptN^2 + alpN^2))

      if(FzL >= 0)
            FyL=0;
      end

      if(FzR >= 0)
            FyR=0;
      end

      if(FzN >= 0)
            FyN=0;
      end

    else
        alpL = 0
        alpR = 0
        alpN = 0

        FxL = 0;
        FxR = 0;
        FxN = 0;
        FyL = 0;
        FyR = 0;
        FyN = 0;
        FzL = 0;
        FzR = 0;
        FzN = 0;

    end

    #Total reaction forces on the Aircraft exerted by ground in the Earth Frame
    FX    = FxL + FxR + FxN*cos(steer) + FyN*sin(steer);
    FY    = FyR + FyL + FyN*cos(steer) + FxN*sin(steer);
    FZ    = FzL + FzR + FzN;

    #Converting FZ from earth to body frame
    Fz_b  = dcm'*[0,0,FZ];

    #Adding the all the Force components in the Body Frame
    FX    = FX + Fz_b[1];
    FY    = FY + Fz_b[2];
    FZ    = Fz_b[3];

    epos = dcm*rPos

    lxLe = epos[1,1];
    lxRe = epos[1,2];
    lxNe = epos[1,3];

    #Total Moment on the Aircraft body frame
    MX  = lyL*(FzL) + lyR*(FzR) + lzL*(-FyL) + lzL*(-FyR) + lzN*(-FyN)*cos(steer) + lzN*(-FxN)*sin(steer);
    MY  = lxNe*(-FzN) + lxRe*(-FzR) + lxLe*(-FzL) + lzN*(FxN)*cos(steer) + lzN*(-FyN)*sin(steer) + lzR*FxR + lzL*FxL;
    MZ  = lyR*(-FxR) + lyL*(-FxL) + lxRe*(FyR) + lxLe*(FyL) + lxNe*(FyN)*cos(steer) + lxNe*(FxN)*sin(steer);

    #Force and Moment on the aircraft due  to the undercarriage
    FMgear = [FX,FY,FZ,MX,MY,MZ];

               
    # Rolling function coefficient
    mu_r = 0.002

    #Wheel dynamics
    if ub > 0
        wLdot = (FxL*Rwheel + BrakeL + Rwheel * abs(FzL) * mu_r )/Iwheel;
        wRdot = (FxR*Rwheel + BrakeR + Rwheel * abs(FzR) * mu_r )/Iwheel;
        wNdot = (FxN*Rwheel + Rwheel * abs(FzN) * mu_r)/Iwheel;
    else
        wLdot = 0;
        wRdot = 0;
        wNdot = 0;
    end

    if(FzL >= 0)
        wLdot=0;
    end

    if(FzR >= 0)
        wRdot=0;
    end

    if(FzN >= 0)
        wNdot=0;
    end

    return FMgear,wLdot,wRdot,wNdot
end
