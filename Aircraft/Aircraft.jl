type Aircraft_t
    M   ::Float64
    W   ::Float64
    c   ::Float64
    b   ::Float64
    Sref::Float64
    Ixx ::Float64
    Iyy ::Float64
    Izz ::Float64
    Ixz ::Float64

    lxL::Float64
    lyL::Float64
    lzL::Float64

    lxR::Float64
    lyR::Float64
    lzR::Float64

    lxN::Float64
    lyN::Float64
    lzN::Float64

    k_L::Float64
    k_R::Float64
    k_N::Float64

    c_L::Float64
    c_R::Float64
    c_N::Float64

    mu ::Float64

    Tmax::Float64

    Tmass::Float64
    Tradius::Float64

    Bp::Float64
    Cp::Float64
    Dp::Float64

    BrakeT::Float64

    c1::Float64
    c2::Float64
    c3::Float64
    c4::Float64
    c5::Float64
    c6::Float64
    c7::Float64
    c8::Float64
    c9::Float64

    t::Array{Float64,1}

    Aircraft_t() = new()
end

function InitAircraft!(Aircraft::Aircraft_t,filename::ASCIIString)


    File = open(filename)
    Ln   = readline(File)

    Aircraft.M    = parse(Float64,split(readline(File))[2])
    Aircraft.c    = parse(Float64,split(readline(File))[2])
    Aircraft.b    = parse(Float64,split(readline(File))[2])
    Aircraft.Sref = parse(Float64,split(readline(File))[2])
    Aircraft.Ixx  = parse(Float64,split(readline(File))[2])
    Aircraft.Iyy  = parse(Float64,split(readline(File))[2])
    Aircraft.Izz  = parse(Float64,split(readline(File))[2])
    Aircraft.Ixz  = parse(Float64,split(readline(File))[2])

    Aircraft.lxL  = parse(Float64,split(readline(File))[2])
    Aircraft.lyL  = parse(Float64,split(readline(File))[2])
    Aircraft.lzL  = parse(Float64,split(readline(File))[2])

    Aircraft.lxR  = Aircraft.lxL
    Aircraft.lyR  = -Aircraft.lyL
    Aircraft.lzR  = Aircraft.lzL

    Aircraft.lxN  = parse(Float64,split(readline(File))[2])
    Aircraft.lyN  = parse(Float64,split(readline(File))[2])
    Aircraft.lzN  = parse(Float64,split(readline(File))[2])

    Aircraft.k_L  = parse(Float64,split(readline(File))[2])
    Aircraft.k_R  = Aircraft.k_L
    Aircraft.k_N  = parse(Float64,split(readline(File))[2])

    Aircraft.c_L  = parse(Float64,split(readline(File))[2])
    Aircraft.c_R  = Aircraft.c_L
    Aircraft.c_N  = parse(Float64,split(readline(File))[2])

    Aircraft.mu      = parse(Float64,split(readline(File))[2])
    Aircraft.Tmax    = parse(Float64,split(readline(File))[2])
    Aircraft.Tmass   = parse(Float64,split(readline(File))[2])
    Aircraft.Tradius = parse(Float64,split(readline(File))[2])
    Aircraft.Bp      = parse(Float64,split(readline(File))[2])
    Aircraft.Cp      = parse(Float64,split(readline(File))[2])
    Aircraft.Dp      = parse(Float64,split(readline(File))[2])
    Aircraft.BrakeT  = parse(Float64,split(readline(File))[2])

    Aircraft.W    = Aircraft.M*9.8

    Ixx = Aircraft.Ixx
    Iyy = Aircraft.Iyy
    Izz = Aircraft.Izz
    Ixz = Aircraft.Ixz

    # Moment of inertial based coefficients
    Aircraft.c1   = ((Iyy-Izz)*Izz-Ixz^2)/(Ixx*Izz-Ixz^2);
    Aircraft.c2   = (Ixx-Iyy+Izz)*Ixz/(Ixx*Izz-Ixz^2);
    Aircraft.c3   = Izz/(Ixx*Izz-Ixz^2);
    Aircraft.c4   = Ixz/(Ixx*Izz-Ixz^2);
    Aircraft.c5   = (Izz-Ixx)/Iyy;
    Aircraft.c6   = Ixz/Iyy;
    Aircraft.c7   = 1/Iyy;
    Aircraft.c8   = ((Ixx-Iyy)*Ixx-Ixz^2)/(Ixx*Izz-Ixz^2);
    Aircraft.c9   = Ixx/(Ixx*Izz-Ixz^2);

    Ln = readline(File)
    Aircraft.t = zeros(45)
    for i=1:45
        Aircraft.t[i] = parse(Float64,readline(File))

    end

    close(File)
end

function DCM(ϕ,θ,ψ)

    #=
    Rotation matrix from body to earth frame
    =#

    R       = zeros(3,3);

    R[1,1]  = cos(θ)*cos(ψ)
    R[1,2]  = sin(ϕ)*sin(θ)*cos(ψ) - cos(ϕ)*sin(ψ)
    R[1,3]  = cos(ϕ)*sin(θ)*cos(ψ) + sin(ϕ)*sin(ψ)
    R[2,1]  = cos(θ)*sin(ψ)
    R[2,2]  = sin(ϕ)*sin(θ)*sin(ψ) + cos(ϕ)*cos(ψ)
    R[2,3]  = cos(ϕ)*sin(θ)*sin(ψ) - sin(ϕ)*cos(ψ)
    R[3,1]  =-sin(θ)
    R[3,2]  = sin(ϕ)*cos(θ)
    R[3,3]  = cos(ϕ)*cos(θ)

    return R
end

include("Aerodynamics.jl")
include("LandingGear.jl")
include("AircraftDynamics.jl")

