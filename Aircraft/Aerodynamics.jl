function GenCoeff(Aircraft::Aircraft_t,α::Float64,β::Float64,
                  pt::Float64,qt::Float64,rt::Float64,
                  δₑ::Float64,δₐ::Float64,δᵣ::Float64)
    #=
    Function to generate aerodynamic coefficients
    - based off Jared and Morelli's paper
    returns:
    C = [C_D, C_E, C_L, C_l, C_m, C_n, C_X, C_Y, C_Z]
    =#

    C_Dv = [1, α, α*qt, α*δₑ,α^2, α^2*qt, α^2*δₑ,
            α^3, α^3*qt, α^4];

    C_Yv = [β, pt, rt, δₐ, δᵣ]

    C_Lv = [1, α, qt, δₑ, α*qt, α^2, α^3, α^4]

    C_lv = [β, pt, rt, δₐ, δᵣ]

    C_mv = [1, α, qt, δₑ, α*qt, α^2*qt, α^2*δₑ, α^3*qt,
            α^3*δₑ, α^4]

    C_nv = [β, pt, rt, δₐ, δᵣ, β^2, β^3];


    C_D = C_Dv'*Aircraft.t[1:10];
    C_Y = C_Yv'*Aircraft.t[11:15];
    C_L = C_Lv'*Aircraft.t[16:23];
    C_l = C_lv'*Aircraft.t[24:28];
    C_m = C_mv'*Aircraft.t[29:38];
    C_n = C_nv'*Aircraft.t[39:45];

    C_E = (C_Y + sin(β)*C_D)/-cos(β);
    C_X = -cos(α)*cos(β)*C_D+cos(α)*sin(β)*C_E+sin(α)*C_L;
    C_Z = -sin(α)*cos(β)*C_D+sin(α)*sin(β)*C_E-cos(α)*C_L;

    C = [C_D[:];C_E[:];C_L[:];C_l[:];C_m[:];C_n[:];C_X[:];C_Y[:];C_Z[:]]

    return C
end
