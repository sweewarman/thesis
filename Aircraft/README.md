Aircraft
========

This is a library to simulate aircraft dynamics. It includes oleo strut and wheel dynamics. The wheel and oleo strut dynamics can be turned off by setting the landing gear flag to zero. Engine failure can also be simulated by setting the engine failure flag. See TestAircraft.jl for example. Aerodynamic and aircraft model parameters are specified with a file inside PARAMS/. Aerodynamic models in this work were chosen from Grauer and Morelli.