
function ACDynamics(Aircraft::Aircraft_t,x::Array{Float64,1},u::Array{Float64,1},ud,Wind)


    xdot  = zeros(16)
    #=
    Dynamics used for simulation
    =#
    r2d = 180/π
    d2r = π/180
    U  = x[1];   #  body velocity x component
    V  = x[2];   #  body velocity y component
    W  = x[3];   #  body velocity z component
    P  = x[4];   #  angular rate about x
    Q  = x[5];   #  angular rate about y
    R  = x[6];   #  angular rate about z

    ϕ  = x[7];   #  roll
    θ  = x[8];   #  pitch
    ψ  = x[9];   #  yaw

    Xₑ = x[10];   #  x position
    Yₑ = x[11];   #  y position
    h  = x[12];   #  Altitude

    wL = x[14]  # angular velocity - left wheel
    wR = x[15]  # angular velocity - right wheel
    wN = x[16]  # angular velocity - nose wheel

    g  = 9.8     #  Acceleration due to gravity in

    LG  = ud[1]

    eng = ud[2] 

    # Impose tail strike constraints
    if LG == 1
        if x[8]*r2d > 11 && x[12] < 0.5
            x[8] = 11*d2r
        end

        if x[8]*r2d < -3 && x[12] < 0.5
            x[8] = -3*d2r
        end
    end

    if x[12] < -1.5
        x[12] = -1.5
    end


    δₑ  = u[1];   #  elevator
    δₐ  = u[2];   #  aileron
    δᵣ  = u[3];    #  rudder
    T   = u[4]*Aircraft.Tmax;  #  throttle

    if eng < 2
        if eng < 1
            T = 0
        else
            T = T/2
        end
    end

    WindB = DCM(ϕ,θ,ψ)'*Wind

    U = U - WindB[1]
    V = V - WindB[2]
    W = W - WindB[3]

    S     = Aircraft.Sref
    M     = Aircraft.M
    c     = Aircraft.c
    b     = Aircraft.b

    c1    = Aircraft.c1
    c2    = Aircraft.c2
    c3    = Aircraft.c3
    c4    = Aircraft.c4
    c5    = Aircraft.c5
    c6    = Aircraft.c6
    c7    = Aircraft.c7
    c8    = Aircraft.c8
    c9    = Aircraft.c9

    heng  = 0;

    
    
    Tstar = 1 - 0.703*10.0^(-5)*abs(h) * 3.28084;
    ρ   = 0.002377*Tstar^(4.14) * 515.4;
    
    # True airspeed
    U0    = sqrt(U^2 + V^2 + W^2)

    # Start from 1 if U = 0 (To prevent singularities)
    if U == 0.0
        U  = 1
        U0 = 1
    end

    # Angle of attack
    α = atan(W/U);

    # Side slip angle
    β  = asin(V/U0);

    # Dynamic pressure
    q     = 0.5*ρ*U0^2;

    pt    = b/(2*U0)*x[4];
    qt    = c/(2*U0)*x[5];
    rt    = b/(2*U0)*x[6];

    C         = GenCoeff(Aircraft,α,β,pt,qt,rt,δₑ,δₐ,δᵣ)
    
    (Gear,wLdot,wRdot,wNdot) = GearFM(Aircraft,x,u,wL,wR,wN,LG)

    udot      = R*V - Q*W - g*sin(θ)        + q*S*C[7]/M + T/M + Gear[1]/M ;
    vdot      = P*W - R*U + g*cos(θ)*sin(ϕ) + q*S*C[8]/M + Gear[2]/M ;
    wdot      = Q*U - P*V + g*cos(θ)*cos(ϕ) + q*S*C[9]/M + Gear[3]/M ;

    pdot      = (c1*R + c2*P + c4*heng)*Q + q*S*b*(c3*C[4] + c4*C[6]) + c3*Gear[4] + c4*Gear[6];
    qdot      = (c5*P - c7*heng)*R        - c6*(P^2 - R^2) + q*S*c*c7*C[5] + c7*Gear[5];
    rdot      = (c8*P - c2*R + c9*heng)*Q + q*S*b*(c4*C[4] + c9*C[6]) + c4*Gear[4] + c9*Gear[6];

    phidot    = P + tan(θ)*(Q*sin(ϕ) + R*cos(ϕ));
    thetadot  = Q * cos(ϕ) - R*sin(ϕ);
    psidot    = (Q*sin(ϕ) + R*cos(ϕ))/cos(θ);

    xedot     = U*cos(ψ)*cos(θ) +
                V*(cos(ψ)*sin(θ)*sin(ϕ) - sin(ψ)*cos(ϕ)) +
                W*(cos(ψ)*sin(θ)*sin(ϕ) + sin(ψ)*sin(ϕ)) 

    yedot     = U*sin(ψ)*cos(θ) +
                V*(sin(ψ)*sin(θ)*sin(ϕ) + cos(ψ)*cos(ϕ)) +
                 W*(sin(ψ)*sin(θ)*cos(ϕ) - cos(ψ)*sin(ϕ)) 

    hdot      = U*sin(θ) - V*cos(θ)*sin(ϕ) - W*cos(θ)*cos(ϕ) 

    U0dot     = (U*udot + V*vdot + W*wdot)/U0

    # Derivative
    xdot[:]     = [udot,vdot,wdot,pdot,qdot,rdot,phidot,thetadot,
                  psidot,xedot,yedot,hdot,U0dot,wLdot,wRdot,wNdot]

    return xdot

end

