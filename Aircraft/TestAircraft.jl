
using PyPlot

include("Aircraft.jl")

# Initialize aircraft model
GTM = Aircraft_t()

# Read aircraft model parameters
InitAircraft!(GTM,"PARAMS/GTM.dat") 

r2d   = 180/π

Tfinal = 40                    # Simulation time span
dt     = 0.01                  # Time step
tspan  = collect(0:dt:Tfinal)  
N      = size(tspan,1)         # Size of tspan
X      = zeros(N,16)           # Array to hold aircraft state variables 
U      = zeros(N,7)            # Array to hold control inputs
Modes  = zeros(N,2)            # Array to hold mode status (pilot vs envelope-aware control)
States = zeros(N,2)            # Array to hold FSAM states

# Set initial condition
x      = zeros(16)              
x[1]   = 0.5
X[1,:] = x[:]
U0     = [0,0,0,0]

# Forward propogate discrete time dynamics
for i=1:N-1

    x[:] = X[i,:]
    
    # Control input (elevator, aileron, rudder, throttle,
    # left brake, right brake, steering)
    # Thrust and brake input are normalized between 0 and 1
    u = [0.0,0.0,0.0,1.0,0.0,0.0,0.0]

    # Wind vector in an inertial reference frame
    Wind = [0,0,0]

    # Discrete inputs (1. Landing gear, 2. Engines operative )
    # Landing gear (0 - off, 1 - on)
    # Engine operative (0 - none, 1 single engine failure) Thrust reduction assumes twin engines.

    ud      = [1,2]
    dX      = ACDynamics(GTM,x,u,ud,Wind)
    X[i+1,:]       = x[:] + dX[:]*dt
    U[i,:]         = u[:]
    U[i+1,:]       = u[:] # Just to fill up the last entry (for plotting)
end


# Plot states and controls
figure(1)
subplot(411)
plot(tspan,X[:,13])
ylabel("Airspeed (m/s)")
subplot(412)
plot(tspan,X[:,12])
ylabel("Altitude (m)")
subplot(413)
plot(tspan,X[:,8]*r2d)
ylabel("Pitch(deg)")
subplot(414)
plot(tspan,X[:,11])
ylabel("Y (m)")

figure(2)
subplot(411)
plot(tspan,U[:,1]*r2d)
ylabel("ele (deg)")
subplot(412)
plot(tspan,U[:,2]*r2d)
ylabel("ail (deg)")
subplot(413)
plot(tspan,U[:,3]*r2d)
ylabel("rud (deg)")
subplot(414)
plot(tspan,U[:,4])
ylabel("thrt ")


