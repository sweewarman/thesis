#===========================================================
 This file initializes all the objects are parameters
 required for Monte carlo simulations for the MDP
===========================================================#

include("../../Aircraft/Aircraft.jl")
include("../../Controller/PilotMDP.jl")
include("../../Controller/EAcontrollerMDP.jl")

# Initialize aircraft model
GTM    = Aircraft_t()
InitAircraft!(GTM,"../../Aircraft/PARAMS/GTM.dat")

# Initialize pilot model
Pilot  = Controller_t()
InitController!(Pilot,"../../Controller/PARAMS/PilotParams.txt")

# Initialize envelope-aware controller
EA     = Controller_t()
InitController!(EA,"../../Controller/PARAMS/EAParams.txt")
VXparams = readdlm("../../AppxModel/PARAMS/GTM_VXenv.dat")

r2d    = 180/π
d2r    = π/180

# Simulation parameters
trials = 1000                   # Number of monte carlo trials
Tfinal = 50                     # Simulation time span
dt     = 0.01                   # Discrete dynamics sampling time
tspan  = collect(0:dt:Tfinal)   # Time series
wind   = [0.0,0.0,0.0]          # Wind vector

N      = size(tspan,1)         
X      = zeros(N,16,trials)
U      = zeros(N,7,trials)
D      = zeros(N,2,trials)
TFail  = zeros(trials,1);
EFail  = zeros(trials,1);
Tend   = ones(trials)*N         # Array to hold stopping times
#==============================
 Parameters to define sampling
==============================#

# Initial conditions
Vinit  = [0.1,20]
Xinit  = [0,100]

# Engine failure distribution
EngFailDist = [0.0, 0.2, 0.8]

# RTO distribution
RTODist = [0.0,1.0]

# Stead wind
Wind   = [0.0,0.0,0.0]

# Pilot parameters

# Proportional gain
kpmu    = -3.0
kpvar   =  2.0
kp_lims = [-0.5,-5.0]

# Rotation speed
vrmu    = 50
vrvar   = 10
vr_lims = [40,60]

# Reference pitch
pmu     = 8
pvar    = 5
p_lims  = [0,15]

# Thrust limits
T_lims  =[0.8,1.0]

#==================================
Parameters required for abstraction
==================================#

Vef0 = 39.58
V1   = 53
V2   = 62.8
Vab0 = 70.25
X_V1 = 713

P_1  = -5
P_2  = 3
P_3  = 10
P_4  = 11

H_TS = 0.5
Rmax = 1500

#=================================
 Define MDP state feature sizes
==================================#

FeatSize = [18, 8, 2, 3]   # [VX,PH,T,E]
TotalN   = prod(FeatSize)  # Total size of MDP statespace

# Transition matrices
TM_P     = zeros(TotalN,TotalN)
TM_EA    = zeros(TotalN,TotalN)
