function sub2ind(feat_size,feat_ind)

    #================================================================
    Get the single index corresponding to the feature indices given
    ================================================================#

    total_feat = length(feat_ind)

    index = 0

    for i in range(1,total_feat)
        if i < total_feat
            index = index +  (feat_ind[i]-1) * prod(feat_size[i+1:end])
        else
            index = index + (feat_ind[i]-1)
        end
    end

    return (index+1)


end

function ind2sub(feat_size,index)

    #================================================================
    Get the feature indices corresponding to the given index
    ================================================================#

    last = length(feat_size);

    subscripts = zeros(Int,length(feat_size))

    index = index - 1

    while last>=1
        subscripts[last]   = (index % feat_size[last]) + 1
        index              = floor(Int,index / feat_size[last])
        last               = last - 1;
    end

    return subscripts

end

# V-X Envelope parametrization
VXmodel(X,params) = params[1] + params[2].*X + params[3].*X.^2 + 
                                       params[4].*X.^3

function AbstractVX(V,X)

    xenv1 = VXmodel(V,VXparams[:,1])
    xenv2 = VXmodel(V,VXparams[:,2])
    xenv3 = VXmodel(V,VXparams[:,3])

    if 0 <= V < Vef0
        if X <= xenv1
            vx = 1
        elseif X > xenv1 && X <= X_V1
            vx = 8
        elseif X > X_V1 && X <= xenv2
            vx = 10
        elseif X > xenv2 && X <= Rmax
            vx = 12
        elseif X > Rmax
            vx = 15
        end
    elseif Vef0 <= V < V1
        if X <= xenv3
            vx = 3
        elseif X > xenv3 && X <= xenv1
            vx = 2
        elseif X > xenv1 && X <= X_V1
            vx = 9
        elseif X > X_V1 && X <= xenv2
            vx = 11
        elseif X > xenv2 && X <= Rmax
            vx = 13
        elseif X > Rmax
            vx = 15
        end
    elseif V1 <= V < max(V2,Vab0)
        if  X <= xenv2
            vx = 4
        elseif X > xenv2 && X <= X_V1
            vx = 5
        elseif X > X_V1 && X < xenv3
            vx = 6
        elseif X > xenv3 && X < Rmax
            vx = 14
        elseif X > Rmax && X > xenv3
            vx = 16
        end
    elseif V > max(V2,Vab0)
        vx = 7
    else
        vx = 18
    end
    
end

function AbstractPH(P,H)

    if H < H_TS
        if P_1 <= P < P_2
            ph = 1
        elseif P_2 <= P < P_3
            ph = 2
        elseif P_3 <= P < P_4
            ph = 3
        elseif P >= P_4
            ph = 4
        end
    else
        if P_1 <= P < P_2
            ph = 5
        elseif P_2 <= P < P_3
            ph = 6
        elseif P_3 <= P < P_4
            ph = 7
        elseif P >= P_4
            ph = 8
        end
    end
        
end

function Abstraction(X,U,D)
    #=================================================================
    Abstraction(X,U,D)
    Inputs:
     * X - Aircraft state vector
     * U - Control input vector
     * D - Discrete variables
    Output:
     * Function returns the discrete state index corresponding to
       the given continuous state and control input
    =================================================================#

    d2r = π/180
    r2d = 180/π

    p = X[4]*r2d
    q = X[5]*r2d
    r = X[6]*r2d
    ϕ = X[7]*r2d
    θ = X[8]*r2d
    ψ = X[9]*r2d
    d = X[10]
    y = X[11]
    h = X[12]
    V = X[13]

    T = U[4]

    eng = D[1]

    # Abstract VX
    vxs = AbstractVX(V,d)

    # Abstract PH
    phs = AbstractPH(θ,h)
    
    # Abstract T
    if T< 0.1
        ts = 1
    else
        ts = 2
    end

    # Abstract Engine
    if eng == 2
        es = 3
    elseif eng == 1
        es = 2
    else
        es = 1
    end

    # Find discrete state
    dis_state = sub2ind(FeatSize,[vxs,phs,ts,es])

    return dis_state

end

function GetTransMat!(X,U,D,Tend,TM)
    #=================================================================
    Estimates the transition probability matrix given the data.
    =================================================================#

   
    Trials   = size(X,3)

    for i=1:Trials
        tf = round(Int,Tend[i])
        
        for j=1:tf-1

            s1 = Abstraction(X[j,:,i],U[j,:,i],D[j,:,i])
            s2 = Abstraction(X[j+1,:,i],U[j+1,:,i],D[j+1,:,i])

            if ( s1 != s2 )
                TM[s1,s2] = TM[s1,s2] + 1
            end

        end
    end

    # Normalize matrix
    for i=1:TotalN
        row_sum = sum(TM[i,:])
        if row_sum > 0
            TM[i,:] = TM[i,:]/row_sum
        end
    end

end
