#====================================================================
This file defines utility functions used for Monte Carlo sampling
=====================================================================#

function InvTransSample(distribution)
    #=================================================================
    Function to sample from an arbitrary distribution using inverse
    transform sampling    
    ==================================================================#
    N = prod(size(distribution))

    # Cumulative distribution
    cum_dist = zeros(N,1)

    for i = 1:N
        cum = sum(distribution[1:i])
        cum_dist[i] = cum
    end

    u = rand()

    if u == 1
        return N
    end

    for i=1:N
        if u <= cum_dist[i]
            return i
        end
    end

end

function TruncatedGaussian(μ,σ,bounds)

    #========================================
    Function that generates a random variable
    according to a normal distribution with
    mean μ and standard deviation σ and then
    ensures the random variable is within the
    specified bounds
    ========================================#

    x = σ*randn() + μ

    xmin = bounds[1]
    xmax = bounds[2]

    # Truncate the gaussian
    if x < xmin
        x = xmin
    elseif x > xmax
        x = xmax
    end
        
    return x
end

function SampleParams()

    # A dictionary to hold all the random variables
    Params = Dict()

    #===============================================
    Sample initial conditions for V, X
     * Sample as much as possible from realistic 
       initial conditions
     * Use exploring starts minimally just to explore
       all regions
    ===============================================#
    V = (Vinit[2] - Vinit[1])*rand() + Vinit[1] 
    X = (Xinit[2] - Xinit[1])*rand() + Xinit[1]
    Params["InitCond"] = [V,X]

    #================================================
    Sample engine status
     * Engine failure state
     * Engine failure time ( which is used only when
       engine failrue state < 2)
    ================================================#
    eng_status   =  InvTransSample(EngFailDist)
    engfail_time =  Tfinal*rand()

    #================================================
    Arbitrary rejected takeoff scenarios
    ================================================#
    rto = InvTransSample(RTODist);

    Params["Engine"] = [eng_status,engfail_time,rto]


    #================================================
    Sample wind speed
     * Sample steady wind speed here and 
     * Sample wind gusts within the simulation loop at 
       every time step
    =================================================#
    wind = [0.0,0.0,0.0]  # for now
    Params["Wind"] = Wind

    #=================================================
    Sample pilot input paramters
    =================================================#
    kp   = TruncatedGaussian(kpmu,kpvar,kp_lims)
    vr   = TruncatedGaussian(vrmu,vrvar,vr_lims)
    pref = TruncatedGaussian(pmu,pvar,p_lims)
    Tmax = (T_lims[2]-T_lims[1])*rand() + T_lims[1]
    Params["Pilot"] = [kp,vr,pref,Tmax]


    


    return Params
end
