#===================================================================
This file defines the key function that runs the Monte Carlo trials
====================================================================#
include("Setup.jl")
include("SamplingUtilities.jl")

function RunMCSim(mode)
    
    for j=1:trials

        # Reset all the data structures to zero
        X[:,:,j] = zeros(N,16)
        U[:,:,j] = zeros(N,7)
        D[:,:,j] = zeros(N,2)
        TFail[j] = 0
        EFail[j] = 3

        # Sample parameters
        Params   = SampleParams()
        
        # Initialize x vector
        x        = zeros(16)

        # Set sampled initial conditions 
        x[1]     = Params["InitCond"][1] 
        x[10]    = Params["InitCond"][2]
        x[13]    = x[1]

        # Landing gear should be down for takeoff
        LG       = 1          
       
        # Set engine parameters
        eng0     = Params["Engine"][1]
        tfail    = Params["Engine"][2]
        rto      = Params["Engine"][3]

        # Set pilot parameters
        Pilot.kpe   = Params["Pilot"][1]
        Pilot.Vᵣ    = Params["Pilot"][2]
        Pilot.θ_ref = Params["Pilot"][3]
        Pilot.Tref  = Params["Pilot"][4]
        
        # Set steady wind here and sample gust for each time step
        wind  = Params["Wind"]

        TFail[j] = tfail
        EFail[j] = eng0

        if(rto == 1)
            EFail[j] = 1
        end

        X[1,:,j] = x[:]

        for i=1:N-1
          
            time = i*dt

            x[:]   = X[i,:,j]
            
            # Initialize engine failure by when t>tfail according to eng0
            if time > tfail
                if eng0 == 2
                    eng = 1
                elseif eng0 == 1
                    eng = 0
                else
                    eng = 2
                end
                
                if rto == 1
                    # Set all engines inoperative if RTO is true
                    eng = 0
                end
            else
                eng = 2
            end

            # Assemble discrete inputs
            xh = vcat(x,eng)
            ud = [LG,eng]

            # Get input
            if mode == 1
                u = PilotControl(GTM,xh,Pilot)
            else
                u = EAControl(GTM,xh,EA)
            end

            # Apply saturation limits
            CheckSaturation(u)

            # Propagate dynamics
            dX             = ACDynamics(GTM,x,u,ud,wind)
            X[i+1,:,j]     = x[:] + dX[:]*dt
            U[i,:,j]       = u[:]
            D[i,1,j]       = eng

            # Tail strike constraint
            if (X[i+1,8,j]*r2d >= 11) && (X[i+1,12,j] < 0.5)
                X[i+1,8,j] = 11 * d2r
            end

            # Make sure airspeed doesn't go below 0.5 when thrust is 0
            if u[4] < 0.1 && x[13] < 0.5
                Tend[j] = i
		U[i+1,:,j] = U[i,:,j]
                break;
            end

	    # Prevent simulation from running too long
	    if x[13] > 90 || x[10] > 1800
	       Tend[j] = i
	       U[i+1,:,j] = U[i,:,j]
               break;
	    end
            
            U[i+1,:,j] = U[i,:,j]
        end

        
    end
    
end
