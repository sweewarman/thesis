using JLD

include("MonteCarlo.jl")
include("Abstraction.jl")
include("PlotMCSim.jl")


# Run Monte carlo trials with pilot
RunMCSim(1)
save("./PilotData.jld","X",X,"U",U,"D",D,"Tend",Tend)

#PlotSim()
GetTransMat!(X,U,D,Tend,TM_P)

# Run Monte carlo trials with envelope-aware controlle
RunMCSim(2)
save("./EAData.jld","X",X,"U",U,"D",D,"Tend",Tend)

#for i=1:trials
    #PlotSim(i)
#end

GetTransMat!(X,U,D,Tend,TM_EA)

#===============================================
Use Pilot and Envelope-aware transition matrices
to construct transition matrices for actions
NOOP and TOGL
===============================================#


TM_NOOP = zeros(TotalN*2,TotalN*2)
TM_TOGL = zeros(TotalN*2,TotalN*2)

TM_NOOP[1:TotalN,1:TotalN] = TM_P[:,:]
TM_NOOP[TotalN+1:2*TotalN,
        TotalN+1:2*TotalN] = TM_EA[:,:]

TM_TOGL[1:TotalN,TotalN+1:2*TotalN] = TM_EA[:,:]
TM_TOGL[TotalN+1:2*TotalN,1:TotalN] = TM_P[:,:]

writedlm("TensorNOOP.txt",TM_NOOP)
writedlm("TensorTOGL.txt",TM_TOGL)

