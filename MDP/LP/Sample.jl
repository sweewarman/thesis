include("MDP_LP.jl")

# MDP problem description

# Get transition matrices
Tensor = Dict()
Tensor["1"] = readdlm("Tensor1.txt")
Tensor["2"] = readdlm("Tensor2.txt")

# Get reward function
Rewards = Dict()
Rewards["1"]   = readdlm("Rewards1.txt")
Rewards["2"]   = readdlm("Rewards2.txt")

# State space description
States = Dict()
States["#states"]  = size(Tensor["1"],1)
States["FeatSize"] = [2,162]

Actions = Dict()
Actions["#actions"] = 2

Constraints = Dict()
Constraints["#constraints"] = 1
Constraints["States"] = [52]

# Initial condition
init = 1
λ = 0.7

VXpolicy = CMDP(States,Actions,
                Tensor,Rewards,
                λ,init,zeros(3),Constraints)

(Values,Policy) = SolveCMDP(VXpolicy);

TotalStates = States["#states"]

MC = zeros(TotalStates,TotalStates)

for i = 1:TotalStates
   ind = Policy[i]
   MC[i,:] =  Tensor["$(ind)"][i,:] 
end
