using Convex
using SCS

type CMDP

    States::Dict{Any,Any}        # Dictionary describing states
    Actions::Dict{Any,Any}       # Dictionary describing actions

    Tensor::Dict{Any,Any}        # Dictionary for Tensor
    Rewards::Dict{Any,Any}       # Dictionary for Rewards

    λ::Float64                   # Discount factor

    InitState::Int               # Initial state
    InitStateArray::Array{Int,1} # Initial features

    Constraints::Dict{Any,Any}   # Array of state constraints (index of state)

end

function SolveCMDP(problem::CMDP)
    
    Ns = problem.States["#states"]
    Na = problem.Actions["#actions"]

    StateFeat = problem.States["FeatSize"]

    Tensor    = problem.Tensor
    Rewards   = problem.Rewards

    init      = problem.InitState
    Findex    = problem.InitStateArray

    NCon      = problem.Constraints["#constraints"]
    StateCon  = problem.Constraints["States"]    
    
    λ = problem.λ

    if(init == 0)
        init = sub2ind(StateFeat[end:-1:1],Findex[end:-1:1])
    end

    # Note that the variables are the occupational measures
    X = Variable(Ns*Na)

    # Objective
    C = zeros(Ns*Na,1)
    for i=1:Na
        C[(i-1)*Ns+1:i*Ns,:] = Rewards["$(i)"]
    end

    # Equality constraint
    I = zeros(Ns,Ns*Na)
    T = zeros(Ns,Ns*Na)
    for i=1:Na
        I[:,(i-1)*Ns+1:i*Ns] = eye(Ns)
        T[:,(i-1)*Ns+1:i*Ns] = transpose(Tensor["$(i)"])
    end

    Aeq       = I - T
    Beq       = zeros(Ns,1)
    Beq[init] = 1

    if NCon > 0
        for i=1:NCon
            AeqC = zeros(Na,Ns*Na) 
            BeqC = zeros(Na,1)

            for j=1:Na
                # State constraint index
                state_con_id         = (j-1)*Ns + StateCon[i]
                AeqC[j,state_con_id] = 1;

                Aeq = vcat(Aeq,AeqC)
                Beq = vcat(Beq,BeqC)
            end
                
        end
    end
        
    Aineq     = eye(2*Ns)
    Bineq     = zeros(Ns*Na,1)

    Constraint = [Aineq*X >= Bineq;Aeq*X == Beq]
    p = maximize(C'*X,Constraint)

    solve!(p, SCSSolver(verbose=0,max_iters=50000))

    Values = -Constraint[2].dual

    OccMeas = zeros(Ns,Na)
    ActDist = zeros(Ns,Na)
    OptAct  = zeros(Int,Ns)

    sol     = X.value

    for i=1:Na
        OccMeas[:,i] = sol[(i-1)*Ns+1:i*Ns]
    end

    for i=1:Ns
        normalizer = sum(OccMeas[i,:])
        for j=1:Na
            ActDist[i,j] = OccMeas[i,j]/normalizer
        end
        (maxval,index) = findmax(ActDist[i,:])
        OptAct[i] = index
    end

    return Values,OptAct

end
