#====================================================================
 Script to compute Q values for a given state
=====================================================================#
include("Setup.jl")

TensorNOOP  = readdlm("Tensor01.txt")
TensorTOGL  = readdlm("Tensor02.txt")
RewardsNOOP = readdlm("Rewards01.txt")
RewardsTOGL = readdlm("Rewards02.txt")

VIout       = readdlm("output_VI.txt",'|')

Value       = map(Float64,VIout[2:end,9])
gamma       = 0.7

function GetQvalue(sindex)

    #================================================================
    Function to compute the Q values for a given state
    =================================================================#

    Q1 = RewardsNOOP[sindex] + gamma*TensorNOOP[sindex,:]*Value
    Q2 = RewardsTOGL[sindex] + gamma*TensorTOGL[sindex,:]*Value

    return Q1[1],Q2[1]

end




println("Table 3(a)")
(q1,q2) = GetQvalue(193)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(194)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(195)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(196)
@printf("%f,%f\n",q1,q2)


println("Table 3(b)")
(q1,q2) = GetQvalue(481)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(482)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(483)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(484)
@printf("%f,%f\n",q1,q2)

println("Table 5(a)")
(q1,q2) = GetQvalue(253)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(254)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(255)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(256)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(257)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(258)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(259)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(260)
@printf("%f,%f\n",q1,q2)


println("Table 5(b)")
(q1,q2) = GetQvalue(541)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(542)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(543)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(544)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(545)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(546)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(547)
@printf("%f,%f\n",q1,q2)

(q1,q2) = GetQvalue(548)
@printf("%f,%f\n",q1,q2)
