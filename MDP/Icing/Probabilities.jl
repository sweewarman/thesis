#=====================================================================
 This file describes the various distributions
 Refer to SciTech paper for Table numbering
=====================================================================#

ProbV_Tab5a = [0.6 0.4 0.0 0.0;
               0.4 0.1 0.5 0.0;
               0.0 0.1 0.5 0.4;
               0.0 0.0 0.5 0.5]

ProbV_Tab5b = [0.0 1.0 0.0 0.0;
               0.0 0.3 0.7 0.0;
               0.0 0.1 0.5 0.4;
               0.0 0.0 0.5 0.5]

ProbV_Tab6a = [0.7 0.3 0.0 0.0;
               0.5 0.2 0.3 0.0;
               0.0 0.2 0.5 0.3;
               0.0 0.0 0.5 0.5]

ProbV_Tab6b = [0.1 0.9 0.0 0.0;
               0.0 0.3 0.7 0.0;
               0.0 0.1 0.5 0.4;
               0.0 0.0 0.5 0.5]

ProbV_Tab7a = [0.98 0.02 0.00 0.00;
               0.85 0.15 0.00 0.00;
               0.00 0.90 0.10 0.00;
               0.00 0.00 0.90 0.10]

ProbV_Tab7b = [0.4 0.6 0.0 0.0;
               0.2 0.5 0.3 0.0;
               0.0 0.8 0.2 0.4;
               0.0 0.0 0.6 0.4]

Probϕ_Tab8a = [0.80 0.20 0.00;
               0.20 0.30 0.50;
               0.00 0.01 0.99]

Probϕ_Tab8b = [1.0 0.0 0.0;
               1.0 0.0 0.0;
               0.0 1.0 0.0]

ProbV_Tab9a = [0.9 0.1 0.0 0.0;
               0.7 0.2 0.1 0.0;
               0.0 0.8 0.2 0.0;
               0.0 0.0 1.0 0.0]
 
ProbV_Tab9b = [0.0 1.0 0.0 0.0;
               0.0 0.6 0.4 0.0;
               0.0 0.6 0.4 0.0;
               0.0 0.0 1.0 0.0]

ProbV = eye(4)
Probϕ = eye(3)
ProbT = eye(2)
ProbH = eye(2)

ProbF = [0.5 0.0;
         0.0 0.5]

ProbI = [1.0 0.0 0.0;
         0.0 1.0 0.0;
         0.0 0.0 1.0]


function GetProbV(Vj,Vi,ϕ,H,T,F,I,M)


    prob = ProbV[Vi,Vj]

    # Airspeed transitions with pilot mode

    if (M == 1) && (I == 3) && (F == 1)
        if (T ∈ (1,2)) && (H==1) && (ϕ ∈ (1,2))
           prob =  ProbV_Tab5a[Vi,Vj]
        end
    end

    if (M == 2) && (I == 3) && (F == 1)
        if (T ∈ (1,2)) && (H==1) && (ϕ ∈ (1,2))
           prob =  ProbV_Tab5b[Vi,Vj]
        end
    end

    if (M == 1) && (I == 1) && (F == 1)
        if (T == 2) && (H==1) && (ϕ ∈ (1,2))
           prob =  ProbV_Tab6a[Vi,Vj]
        end
    end
    
    if (M == 2) && (I == 1) && (F == 1)
        if (T == 2) && (H==1) && (ϕ ∈ (1,2))
           prob =  ProbV_Tab6b[Vi,Vj]
        end
    end

    if (M==1) && (F==2) && (I==3)
        if (T==1) && (H==2) && (ϕ ∈ (1,2,3))
            prob = ProbV_Tab5a[Vi,Vj]
        end
    end

    if (M==2) && (F==2) && (I==3)
        if (T==1) && (H==2) && (ϕ ∈ (1,2,3))
            prob = ProbV_Tab5b[Vi,Vj]
        end
    end

    
    if (M == 1) && (I == 2) && (F == 1)
        if (T ∈ (1,2)) && (H ∈ (1,2)) && (ϕ ∈ (1,2))
           prob =  ProbV_Tab7a[Vi,Vj]
        end
    end
    
    if (M == 2) && (I == 2) && (F == 1)
        if (T ∈ (1,2)) && (H ∈ (1,2)) && (ϕ ∈ (1,2))
           prob =  ProbV_Tab7b[Vi,Vj]
        end
    end

    if (M == 1) && (I == 3) && (F ∈ (1,2))
        if (T == 1) && (H==2) && (ϕ ∈ (1,2,3))
           prob =  ProbV_Tab9a[Vi,Vj]
        end
    end

    if (M == 2) && (I == 3) && (F ∈ (1,2))
        if (T == 1) && (H==2) && (ϕ ∈ (1,2,3))
           prob =  ProbV_Tab9b[Vi,Vj]
        end
    end

    return prob

end

function GetProbϕ(ϕj,ϕi,V,H,T,F,I,M)
    
    prob = Probϕ[ϕi,ϕj]

    if (M==1) && (F==2) && (I==3)
        if (T==1) && (H==2)
            prob = Probϕ_Tab8a[ϕi,ϕj]
        end
    end

    if (M==2) && (F==2) && (I==3)
        if (T==1) && (H==2)
            prob = Probϕ_Tab8b[ϕi,ϕj]
        end
    end

    return prob

end

