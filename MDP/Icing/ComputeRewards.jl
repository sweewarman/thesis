#=====================================================================
This file defines a function to compute rewards given state and action
=====================================================================#

function GetRewards(V,ϕ,H,T,F,I,M,A)

    # Reward for airspeed states
    if V == 1
        R1 = -1
    else
        R1 = 0
    end

    # Reward for bank angle states
    if ϕ == 3
        R2 = -1
    else
        R2 = 0
    end

    # Rewards for actions
    if (M == 1) && (A == 2)
        R3 = -1
    elseif (M==2) && (A==1)
        R3 = -0.5
    else
        R3 = 0
    end

    w1 = 100
    w2 = 50
    w3 = 10

    R = w1*R1 + w2*R2 + w3*R3
        
    return R
end
