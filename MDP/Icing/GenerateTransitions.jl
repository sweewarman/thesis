#=====================================================================
This script generates the transition matrices to run the MDP f
formulation to prevent LOC due to in-flight icing.

See SciTech'16 paper for description.
See also thesis chapter 4 for more details.
=====================================================================#
include("Setup.jl")
include("Probabilities.jl")
include("ComputeRewards.jl")

# Assemble probability matrix
for i=1:TotalS
    for j=1:TotalS

        # Get feature subscripts corresponding to the linear index
        (Vi,ϕi,Hi,Ti,Fi,Ii,Mi) = ind2sub((lenV,lenϕ,lenH,lenT,lenF,lenI,lenM),i)
        (Vj,ϕj,Hj,Tj,Fj,Ij,Mj) = ind2sub((lenV,lenϕ,lenH,lenT,lenF,lenI,lenM),j)
        
        # Skip if current mode and 
        if Mi != Mj
            continue
        end

        prob = 1


        # Using product rule to estimate the probability entries
        prob = prob * GetProbV(Vj,Vi,ϕi,Hi,Ti,Fi,Ii,Mi)
        prob = prob * GetProbϕ(ϕj,ϕi,Vi,Hi,Ti,Fi,Ii,Mi)
        prob = prob * ProbH[Hi,Hj]
        prob = prob * ProbT[Ti,Tj]
        prob = prob * ProbF[Fi,Fj]
        prob = prob * ProbI[Ii,Ij]

        # Store probability entry in corresponding matrix
        
        TensorNOOP[i,j] = prob
        
    end
end

halfS = round(Int,TotalS/2)

# Construct tensor for actions
TensorTOGL[1:halfS,halfS+1:TotalS] = TensorNOOP[halfS+1:TotalS,
                                                halfS+1:TotalS]
 
TensorTOGL[halfS+1:TotalS,1:halfS] = TensorNOOP[1:halfS,1:halfS]

for i=1:TotalSA

    if i <= TotalS
        (Vi,ϕi,Hi,Ti,Fi,Ii,Mi) = ind2sub((lenV,lenϕ,lenH,lenT,lenF,lenI,lenM),i)
        Rewards1[i] = GetRewards(Vi,ϕi,Hi,Ti,Fi,Ii,Mi,1)
    else
        (Vi,ϕi,Hi,Ti,Fi,Ii,Mi) = ind2sub((lenV,lenϕ,lenH,lenT,lenF,lenI,lenM),i-TotalS)
        Rewards2[i-TotalS] = GetRewards(Vi,ϕi,Hi,Ti,Fi,Ii,Mi,2)
    end

end

# Store Tensors and reward matrices to file
writedlm("Tensor01.txt",TensorNOOP)
writedlm("Tensor02.txt",TensorTOGL)
writedlm("Rewards01.txt",Rewards1)
writedlm("Rewards02.txt",Rewards2)
