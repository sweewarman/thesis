# This script defines the MDP

# Abstracted MDP states

Nf = 7          # Number of state features

V = range(1,4)  # Airspeed
ϕ = range(1,3)  # Bank angle
H = range(1,2)  # Altitude
T = range(1,2)  # Thrust
F = range(1,2)  # Flight plan
I = range(1,3)  # Icing intensity
M = range(1,2)  # Current mode
A = range(1,2)  # Action

# length of all state features
lenV = length(V)
lenϕ = length(ϕ)
lenH = length(H)
lenT = length(T)
lenF = length(F)
lenI = length(I)
lenM = length(M)

# Total number of states in the MDP formulation
TotalS  = lenV*lenϕ*lenH*lenT*lenF*lenI*lenM

# Total states and actions
TotalSA = TotalS * length(A)

# Define matrices to hold tensors and rewards
TensorP = zeros(TotalS,TotalS)
TensorE = zeros(TotalS,TotalS)

TensorNOOP = zeros(TotalS,TotalS)
TensorTOGL = zeros(TotalS,TotalS)

Rewards1 = zeros(TotalS,1)
Rewards2 = zeros(TotalS,1)
