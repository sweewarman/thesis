using PyPlot

function PlotSim()
    #======================================#
    # Function to plot monte carlo simulations
    #======================================#

    # Tail strike constraints

    #=
    x1 = collect(11:1:15)
    y1 = ones(x1)*0.5

    y2 = collect(-1:0.5:0.5)
    x2 = ones(y2)*11
    =#

    # Runway overrun constraints
    x1 = collect(5:1:75)
    y1 = ones(x1)*2000

    y2 = collect(2000:100:2500)
    x2 = ones(y2)*75


    for j=1:trials
        
        # Plot states and controls

        figure(1)
        subplot(321)
        plot(tspan,X[:,13,j][:])
        ylim([0,100])
        ylabel("V")
        subplot(322)
        plot(tspan,X[:,12,j][:])
        ylim([-2,5])
        ylabel("H")
        subplot(323)
        plot(tspan,X[:,8,j][:]*r2d)
        ylim([-5,15])
        ylabel("theta")
        subplot(324)
        plot(tspan,X[:,10,j][:])
        ylabel("X")
        subplot(325)
        plot(tspan,X[:,11,j][:])
        ylabel("Y")
        subplot(326)
        plot(tspan,U[:,4,j][:])
        ylabel("T")

        #=
        figure(2)
        plot(x1,y1,"r--")
        plot(x2,y2,"r--")
        plot(X[:,8,j][:]*r2d,X[:,12,j][:])
        =#

       
        figure(2)
        plot(x1,y1,"r--")
        plot(x2,y2,"r--")
        plot(X[:,13,j][:],X[:,10,j][:])
        

        #=
        figure(3)
        subplot(211)
        plot(tspan,MODES[:,1,j])
        subplot(212)
        plot(tspan,STATES[:,1,j])
        =#
    end
end
