function SampleArb(distribution)

    #=================================================================
    Function to sample from an arbitrary distribution using inverse
    transform sampling
    
    ==================================================================#

    N = prod(size(distribution))

    # Cumulative distribution
    cum_dist = zeros(N,1)

    for i = 1:N
        cum = sum(distribution[1:i])
        cum_dist[i] = cum
    end

    u = rand()

    if u == 1
        return N
    end

    for i=1:N
        if u <= cum_dist[i]
            return i
        end
    end

end

function Truncate(Input,MinVal,MaxVal)

    #=================================================================
    Returns the truncated value of Input if Input exceeds bounds 
    specified by MaxVal or MinVal
    =================================================================#
    
    Output = Input

    if Input < MinVal
        Output = MinVal
    end

    if Input > MaxVal
        Output = MaxVal
    end

    return Output
end

function GaussPdf(μ,var,x)
    
    #=================================================================
    Function returns the density of random variable
    =================================================================#
    
    density  = 1/(sqrt(2*π*var)).*exp(-0.5.*(x-μ)^2./var)

    return density

end


function DistRectangle(Bounds, point)
    #=================================================================
    Function to compute distance of a given point from a rectangular set
    - sign convention used is in accordance with convention described in 
    =================================================================#

    x_min = Bounds[1]
    x_max = Bounds[2]
    y_min = Bounds[3]
    y_max = Bounds[4]

    x     = point[1]
    y     = point[2]

    # Point lies inside rectangle
    if  (x_min <= x <= x_max) && (y_min <= y <= y_max) 
                
        #=
        Compute distance from four boundaries
        and return the shortest distance
        =#

        dist    = zeros(4)
        dist[1] = abs(x - x_min)
        dist[2] = abs(x - x_max)
        dist[3] = abs(y - y_min)
        dist[4] = abs(y - y_max)
        
        
        
        return -minimum(dist)

    else
        
        #=============================================================
        Compute distance from four boundaries and four corners
        and return the shortest distance
        =============================================================#
        
                
        if (x_min <= x <= x_max)
            dist    = zeros(2)
            dist[1] = abs(y - y_min)
            dist[2] = abs(y - y_max)
        elseif( y_min <= y <= y_max)
            dist    = zeros(2)
            dist[1] = abs(x - x_min)
            dist[2] = abs(x - x_max)
        else
            dist    = zeros(4)
            dist[1] = sqrt( (x - x_min)^2 + (y - y_min)^2 ) 
            dist[2] = sqrt( (x - x_min)^2 + (y - y_max)^2 ) 
            dist[3] = sqrt( (x - x_max)^2 + (y - y_min)^2 ) 
            dist[4] = sqrt( (x - x_max)^2 + (y - y_max)^2 ) 
        end

        
        return minimum(dist)
    end
end

function DistRectangle3(Bounds, point)
    #=================================================================
    Function to compute distance of a given point from a rectangular set
    - sign convention used is in accordance with convention described in 
    ==================================================================#

    x_min = Bounds[1]
    x_max = Bounds[2]
    y_min = Bounds[3]
    y_max = Bounds[4]
    z_min = Bounds[5]
    z_max = Bounds[6]

    x     = point[1]
    y     = point[2]
    z     = point[3]

   
    # Point lies inside rectangle
    if  (x_min <= x <= x_max) && (y_min <= y <= y_max) && (z_min <= z <= z_max)
                
        #=============================================================
        Compute distance from four boundaries
        and return the shortest distance
        =============================================================#

        dist    = zeros(6)
        dist[1] = abs(x - x_min)
        dist[2] = abs(x - x_max)
        dist[3] = abs(y - y_min)
        dist[4] = abs(y - y_max)
        dist[5] = abs(z - z_min)
        dist[6] = abs(z - z_max)
        
        
        return -minimum(dist)

    else
        
        #=============================================================
        Compute distance from all surfaces and corners
        and return the shortest distance
        =============================================================#
        
        
        if x_min <= x <= x_max
            dist    = zeros(2)
            dist[1] = abs(y - y_min)
            dist[2] = abs(y - y_max)
        elseif y_min <= y <= y_max
            dist    = zeros(2)
            dist[1] = abs(x - x_min)
            dist[2] = abs(x - x_max)
        elseif (x_min <= x <= x_max) && (y_min <= y <= y_max)
            dist     = zeros(2)
            dist[1]  = abs(z-z_min)
            dist[2] = abs(z-z_max)
        else
            dist    = zeros(8)
            dist[1] = sqrt( (x - x_min)^2 + (y - y_min)^2 + (z - z_min)^2) 
            dist[2] = sqrt( (x - x_min)^2 + (y - y_max)^2 + (z - z_min)^2) 
            dist[3] = sqrt( (x - x_max)^2 + (y - y_min)^2 + (z - z_min)^2) 
            dist[4] = sqrt( (x - x_max)^2 + (y - y_max)^2 + (z - z_min)^2) 
            dist[5] = sqrt( (x - x_min)^2 + (y - y_min)^2 + (z - z_max)^2) 
            dist[6] = sqrt( (x - x_min)^2 + (y - y_max)^2 + (z - z_max)^2) 
            dist[7] = sqrt( (x - x_max)^2 + (y - y_min)^2 + (z - z_max)^2) 
            dist[8] = sqrt( (x - x_max)^2 + (y - y_max)^2 + (z - z_max)^2) 
        end

     
        return minimum(dist)
    end
end

function ComputeLikelihood(Robustness,ParamSampled,
                           Distribution,Behaviors,Reduce,Kparam)
    #=================================================================
    Function to compute the likelihood ratio
    =================================================================#

    N          = Reduce
    Nparams    = size(ParamSampled,2)
    Nsupport   = size(Distribution,2)
    Likelihood = zeros(N,Nparams)
    Omega      = exp(-Kparam*Robustness)
    AuxMat     = hcat(-Omega,ParamSampled)
    SortedMat  = sortrows(AuxMat)

    # Down select only top N samples
    for i=1:Nparams
        for j=1:N
            
            prob = 0
            for k=1:Nsupport
                
                if Behaviors[k][1] <= SortedMat[j,1+i] < Behaviors[k][2] 
                    prob = Distribution[i,k]
                end
            end

            if prob == 0
                @printf("Check probabilities\n")
                #exit()
            end

            # Likelihood is given by robustness/(probability of parameter
            # according to previous distirubtion)
            Likelihood[j,i] = -SortedMat[j,1]/prob

        end
    end


    return SortedMat,Likelihood
end

function ComputeLikelihoodGauss(Robustness,ParamSampled,
                                Behaviors,Reduce,Kparam)
    #=================================================================
    Function to compute the likelihood ratio
    =================================================================#

    N          = Reduce
    Nparams    = size(ParamSampled,2)
    Likelihood = zeros(N,Nparams)
    Omega      = exp(-Kparam*Robustness)
    AuxMat     = hcat(-Omega,ParamSampled)
    SortedMat  = sortrows(AuxMat)

    # Down select only top N samples
    for i=1:Nparams
        for j=1:N
            
            prob = 0
            
            μ    = Behaviors[i][1]
            var  = Behaviors[i][2]
            x    = ParamSampled[j,i]
            prob = GaussPdf(μ,var,x)

            # Likelihood is given by robustness/(probability of parameter
            # according to previous distirubtion)
            Likelihood[j,i] = -SortedMat[j,1]/prob

        end
    end


    return SortedMat,Likelihood
end

function TiltDistribution(Likelihood,SortedMat,Distribution,Behaviors)

    #=================================================================
    Function that provides a new distribution
    =================================================================#


    NewDistribution = zeros(Distribution)

    Nparams  = size(NewDistribution,1)
    NSupport = size(NewDistribution,2)

    m        = size(Likelihood,1)

    for i=1:Nparams
       
        # Denominator sum (constant for a given parameter)
        sum_gamma = sum(Likelihood[:,i])

        # Update cell probabilities for each parameter
        for k = 1:NSupport

            # Numerator sum
            num_sum   = 0

            for j=1:m
                
                if Behaviors[k][1] <= SortedMat[j,1+i] < Behaviors[k][2] 
                    num_sum = num_sum + Likelihood[j,i]
                end
            end
            
            NewDistribution[i,k] = num_sum/sum_gamma
            
        end
 
    end
    
    return NewDistribution
    
end

function TiltDistributionGauss(Likelihood,SortedMat,Behavior)

    #=================================================================
    Function that provides a new distribution
    =================================================================#

    Nparams  = size(Likelihood,2)

    m        = size(Likelihood,1)

    for i=1:Nparams
       
        # Denominator sum (constant for a given parameter)
        sum_gamma = sum(Likelihood[:,i])

        num_sum1  = 0;
        num_sum2  = 0;

        # Mean
        for j=1:m
          num_sum1 = num_sum1 + Likelihood[j,i]*SortedMat[j,1+i]  
        end
            
        Newμ = num_sum1/sum_gamma

        # Variance
        for j=1:m
          num_sum2 = num_sum2 + Likelihood[j,i]*(SortedMat[j,1+i] -Newμ)^2 
        end

        Newvar = num_sum2/sum_gamma
       
        Behavior[i][1] = Newμ
        Behavior[i][2] = Newvar
   end
 

    
end
