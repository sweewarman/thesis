#===================================================================
This file performs falsification of temporal logic properties
====================================================================#
include("Setup.jl")

function RunMCSim()

    for k=1:NumMetaSim
    
        for j=1:trials
            
            eng      = 2     # Both engines are operative
            LG       = 1     # Landing gear is operative
            
            # Set FSAM states to initial state
            fsam.state1 = 1
            fsam.state2 = 1

            # Behavior index and type
            Bid   = 0
            Btype = 0

            # Taxi to beginning of runway
            x        = zeros(16)

            # Initiate airspeed to a non-zero value. 
            x[1]     = 0.2
            x[13]    = 0.2

            X[1,:,j] = x[:]

            for i=1:N-1

                x[:]   = X[i,:,j]
                #=============================#
                # Sample new behavior
                #=============================#
                
                # Find behavior index
                BidNew = floor(Int,i/Btsteps) + 1
                
                # For new behavior index, sample new behavior
                if Bid != BidNew

                    Bid   = BidNew

                    # Sample from available behaviors

                    # Pitch reference
                    Btype = SampleArb(Distribution1[BidNew,:])
                    Pilot.θ_ref  = Behavior1[Btype][1] + (Behavior1[Btype][2] - Behavior1[Btype][1])*rand()                
                    ParamSampled1[j,Bid] = Pilot.θ_ref
                    
                    # Thrust reference
                    Btype = SampleArb(Distribution3[BidNew,:])
                    Pilot.Tref  = Behavior3[Btype][1] + (Behavior3[Btype][2] - Behavior3[Btype][1])*rand()                
                    ParamSampled3[j,Bid] = Pilot.Tref

                    # Yaw reference
                    #Btype = SampleArb(Distribution2[BidNew,:])
                    #Pilot.ψ_ref  = Behavior2[Btype][1] + (Behavior2[Btype][2] - Behavior2[Btype][1])*rand()                
                    #ParamSampled2[j,Bid] = Pilot.ψ_ref
                    
                end
                
                xf             = vcat(x,eng)
                ud             = [LG,eng]

                # Call FSAM engine
                u,mode,state   = FSAM(GTM,xf,Pilot,EA,
                                      PilotControl,EAControl,
                                      fsam)

                MODES[i,:,j] = mode[:]
                STATES[i,:,j] = state[:]
                
                # Apply saturation limits
                CheckSaturation(u)

                # Propagate dynamics
                dX             = ACDynamics(GTM,x,u,ud,wind)
                X[i+1,:,j]     = x[:] + dX[:]*dt
                U[i,:,j]       = u[:]

                # Make sure airspeed doesn't go below 0.5 when thrust is 0
                if u[4] < 0.1 && x[13] < 0.5
                    Tend[j] = i

                    # Compute robustness before termination
                    req_point = [x[13]/vmax,x[10]/xmax,x[12]/hmax]
                    
                    min_rob = DistRectangle3(UnsafeSet,req_point)
                    
                    # Only keep track of minimum value
                    if min_rob < Robustness[j]
                        Robustness[j] = min_rob
                    end
                    
                    break;
                end

                # Tail strike constraint
                if (X[i+1,8,j]*r2d >= 11) && (X[i+1,12,j] < 0.5)
                    X[i+1,8,j] = 11 * d2r
                end
                
                # Compute Robustness

                # Required point
            
                # Use this point for tail strike
                #req_point = [x[8]*r2d/pmax,x[12]/hmax]

                # Use this point for runway overrun
                req_point = [x[13]/vmax,x[10]/xmax,x[12]/hmax]
                
                min_rob = DistRectangle3(UnsafeSet,req_point)

                # Only keep track of minimum value
                if min_rob < Robustness[j]
                    Robustness[j] = min_rob
                end
                
                
            end

            
        end
        
    
        #==========================#
        # Compute Likelihood ratio
        #==========================#
        SortedMat1,Likelihood1 = ComputeLikelihood(Robustness,ParamSampled1,
                                                   Distribution1,Behavior1,Nreduce,Kparam)

        SortedMat3,Likelihood3 = ComputeLikelihood(Robustness,ParamSampled3,
                                                   Distribution3,Behavior3,Nreduce,Kparam)

        #SortedMat2,Likelihood2 = ComputeLikelihood(Robustness,ParamSampled2,
        #                                          Distribution2,Behavior2,Nreduce,Kparam)
        

        #===========================#
        # Perform tilting
        #===========================#

        # Tilt old distribution to get new distribution
        Distribution1[:,:]  = TiltDistribution(Likelihood1,SortedMat1,
                                          Distribution1,Behavior1)

        Distribution3[:,:]  = TiltDistribution(Likelihood3,SortedMat3,
                                          Distribution3,Behavior3)

        #Distribution2[:,:] = TiltDistribution(Likelihood2,SortedMat2,
        #                                   Distribution2,Behavior2)

        
        #=============================#
        # Write distributions to file
        #=============================#
        for i=1:3
            filename = "Dist"*string(i)*"Iter"*string(k)*".csv"
            
            if i==1
                writecsv(filename,Distribution1)
            elseif i==2
                writecsv(filename,Distribution2)
            elseif i==3
                writecsv(filename,Distribution3)
            end
        end

     # Write robustness to file
     filename = "RobustnessIter"*string(k)*".csv"

     writecsv(filename,Robustness)

   end    

end
