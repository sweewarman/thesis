#===========================================================
 This file initializes all the objects are parameters
 required for Monte carlo simulations for the cross-entropy
 falsification problem.
===========================================================#

include("../Aircraft/Aircraft.jl")
include("../Controller/PilotFalsification1.jl")
include("../Controller/EAcontroller.jl")
include("../FSM/FSAM_new.jl")
include("CrossEntropy.jl")

# Initialize aircraft model
GTM    = Aircraft_t()
InitAircraft!(GTM,"../Aircraft/PARAMS/GTM.dat")

# Initialize pilot model
Pilot  = Controller_t()
InitController!(Pilot,"../Controller/PARAMS/PilotParams.txt")

# Initialize envelope-aware controller
EA     = Controller_t()
InitController!(EA,"../Controller/PARAMS/EAParams.txt")

# Initialize FSAM state machine
fsam  = FSM_t()
InitFSAM!(fsam,"../FSM/FSAMParams.txt")

r2d    = 180/π
d2r    = π/180

# Simulation parameters
trials = 1000                    # Number of monte carlo trials
Nreduce = round(Int,trials/10)  # Downselection parameter
Tfinal = 50                     # Simulation time span
dt     = 0.01                   # Discrete dynamics sampling time
tspan  = collect(0:dt:Tfinal)   # Time series

wind   = [0.0,0.0,0.0]          # Wind vector

N      = size(tspan,1)         
X      = zeros(N,16,trials)
U      = zeros(N,7,trials)
MODES  = zeros(N,2,trials)
STATES = zeros(N,2,trials)
Tend   = ones(trials)*N         # Array to hold stopping times

#=========================#
# Cross Entropy parameters
#=========================#

# Number of segments the takeoff phase is to be divided
Nsegments  = 3

# Time steps for each behavior
Btsteps    = ceil(Int,N/Nsegments)

# Definition of behaviors

# Parameters for piece-wise uniform distributions

#=====================================================================
# Pitch guidance
Nbehavior1 = 5
Behavior1  = Dict(3=>[-0.01,0.01],
                  4=>[6,9],
                  2=>[-5,-1],
                  5=>[9,15],
                  1=>[-10,-5])

Distribution1 = ones(Nsegments,Nbehavior1)*(1/Nbehavior1)
ParamSampled1 = zeros(trials,Nsegments)

# Yaw guidance
Nbehavior2 = 5
Behavior2  = Dict(3=>[-0.001,0.001],
                  4=>[2,5],
                  5=>[5,10],
                  2=>[-5,-2],
                  1=>[-10,-5])

Distribution2 = ones(Nsegments,Nbehavior2)*(1/Nbehavior2)
ParamSampled2 = zeros(trials,Nsegments)

# Thrust guidance
Nbehavior3 = 3
Behavior3  = Dict(3=>[0.8,1.0],
                  2=>[0.4,0.8],
                  1=>[0,0.1])
Distribution3 = ones(Nsegments,Nbehavior3)*(1/Nbehavior3)
ParamSampled3 = zeros(trials,Nsegments)
=====================================================================#


#Parameters for gaussian distribution
Distribution1 = ones(Nsegments,2)
Distribution2 = ones(Nsegments,2)
Distribution3 = ones(Nsegments,2)

ParamSampled1 = zeros(trials,Nsegments)
ParamSampled2 = zeros(trials,Nsegments)
ParamSampled3 = zeros(trials,Nsegments)


# Mean and variance of behaviors
# Pitch reference
Behavior1 = Dict(1=>[0.,15.],
                 2=>[0.,15.],
                 3=>[0.,15.])

# Yaw reference
Behavior2 = Dict(1=>[0.,15.],
                 2=>[0.,15.],
                 3=>[0.,15.])

# Thrust command
Behavior3 = Dict(1=>[0.5,0.1],
                 2=>[0.5,0.1],
                 3=>[0.5,0.1])


#====================================================================#

Robustness    = ones(trials)*1e20

# Constants for normalization
vmax = 100
xmax = 2500
hmax = 5
pmax = 15

Pmin = -5
Pmax = 10
Tmin = 0
Tmax = 1

# Constraints are expressed as hyper cubes

# Tail strike constraint
UnsafeSet     = [11.0/pmax,15.0/pmax,
                 -5.0/hmax,0.5/hmax]

# Runway overrun constraint
#UnsafeSet      = [35/vmax,75/vmax,
#                  2000/xmax,2200/xmax,
#                  -5/hmax,0.5/hmax]

# Total number of meta iteration (outer loop)
NumMetaSim     = 7

# K parameter for Omega distribution
Kparam         = 1
