#====================================================================
This file plots the distribution from the cross-entropy 
simulations, the average robustness across all cross-entropy trials,
and a sample monte carlo runs from the selected distributions 

Use ReqIter to select the required cross-entropy trial from which
you would like to view distributions and run monte carlo simulations
====================================================================#

include("MonteCarlo.jl")
include("PlotMCSim.jl")

# Specify directory from which to read data files
DirName = "./NewFSAM_PT/"

trials  = 1000    # Number of trials in each cross-entropy simulation
NumIter = 7       # Number of cross-entropy trials to be read from file
ReqIter = 7       # Required cross-entropy trial to visualize


# Setup variables required

xlabel1 = Array(ASCIIString,5)
xlabel2 = Array(ASCIIString,5)
xlabel3 = Array(ASCIIString,3)

Distributions1 = zeros(Nsegments,Nbehavior1,NumIter)
Distributions2 = zeros(Nsegments,Nbehavior2,NumIter)
Distributions3 = zeros(Nsegments,Nbehavior3,NumIter)

x1 = collect(1:Nbehavior1)
x2 = collect(1:Nbehavior2)
x3 = collect(1:Nbehavior3)

robustness    = zeros(trials,NumIter)
stdrobustness = zeros(NumIter)

Var = zeros(NumIter)
std = zeros(NumIter)
AvgRobustness = zeros(NumIter)

# Create labels for bar plots
for i=1:5
    xlabel1[i] = string(Behavior1[i])
    xlabel2[i] = string(Behavior2[i])

    if(i<=3)
     xlabel3[i] = string(Behavior3[i])
    end
end

# Read all distributions and robustness values
for k=1:NumIter
    for i=1:3
        filename = DirName*"Dist"*string(i)*"Iter"*string(k)*".csv"
        
        data = readcsv(filename)

        if i==1
            Distributions1[:,:,k] = data
        elseif i==2
            Distributions2[:,:,k] = data
        elseif i==3
            Distributions3[:,:,k] = data
        end
    end

    filename = DirName*"RobustnessIter"*string(k)*".csv"
    data = readcsv(filename)
    
    robustness[:,k] = data
end

# Plot distributions for required iteration (bar plots)
figure(4)
for i = 1:Nsegments
    subplot(Nsegments,1,i)
    bar(x1,Distributions1[i,:,ReqIter][:],width=1,align="center",
        alpha=0.6,linewidth=2.0,edgecolor="k")
    xticks(x1,xlabel1)
    axis("tight")
    
end

figure(5)
for i = 1:Nsegments
    subplot(Nsegments,1,i)
    bar(x3,Distributions3[i,:,ReqIter][:],width=1.0,align="center",
        alpha=0.6,linewidth=2.0,edgecolor="k")
    xticks(x3,xlabel3)
end

#=
figure(3)
for i = 1:Nsegments
    subplot(Nsegments,1,i)
    bar(x2,Distributions2[i,:,ReqIter][:],align="center",
       alpha=0.6,linewidth=2.0,edgecolor="k")
    xticks(x2,xlabel2)
end
=#

# Compute average robustness and standard deviation for all trials
# and plot a graph with errror bars
figure(6)
for k=1:NumIter

    AvgRobustness[k] = sum(robustness[:,k])/trials

    for i=1:trials
        Var[k] = Var[k] + (robustness[i,k] - AvgRobustness[k])^2
    end
    
    Var[k] = Var[k]/trials
    std[k] = sqrt(Var[k])

end

x      = collect(1:NumIter)
lerror = std
uerror = std
errs   = vcat(lerror',uerror')

plot(x,AvgRobustness)
errorbar(x,AvgRobustness,yerr=errs,fmt="ro")
ylabel("Average Robustness")
xlabel("Iterations")


# Pitch guidance
Distribution1 = Distributions1[:,:,ReqIter]

# Yaw guidance
Distribution2 = Distributions2[:,:,ReqIter]

# Thrust guidance
Distribution3 = Distributions3[:,:,ReqIter]

#trials = 50
#RunMCSim()
#PlotSim()




